import os
import sys
import scipy.io.wavfile as wf
import scipy.signal as scs
import matplotlib.pyplot as plt
import numpy as np

from worker import *

measloc = range(-3, 3)

FFT_TIMELEN = 0.1
FFT_SLIDE = 0
FREQ_CUTOFF = (50, 2000)
NUMPEAKS = 7
MIN_PEAK_DIST = 4
LOGBASE = 2 ** (1/12.0)
PKTHR1 = 2
FLUSH_AFTER = 2
HIT_MIN_LEN = 8
HIT_MIN_THR = 0.7
PROB_THR = 0.5
VOLUME_THR = 3000
SMOOTH_SIZE = 2
INTERVALS = [100, 200, 400, 800, 1300]
BAND_SEEK_SIZE = 1

def readwave(fn):
    rate, stream = wf.read(fn)
    data0 = stream[:, 0]
    data1 = stream[:, 1]
    assert(len(data0) == len(data1))

    return data0, data1, rate


def make_db_stats(db):
    result = {'relpeaks': {}, 'peaks': {}, 'Eavg': {}, 'maxpeak': {}}
    for i in range(len(INTERVALS) + 1):
        result['band_{0}'.format(i + 1)] = {}

    for k, v in db.items():
        for i, e in enumerate(v):
            for e2 in e:
                todo = []
                print "e2=", e2
                mp2 = int(round(e2[1]['maxpeak'] * muls['maxpeak']))
                todo.append((mp2, 'maxpeak'))

                eavg = int(round(e2[1]['Eavg'] * muls['Eavg']))
                todo.append((eavg, 'Eavg'))

                for e3 in e2[1]['relpeaks']:
                    todo.append((int(round(e3 * muls['relpeaks'])), 'relpeaks'))

                for e3 in e2[1]['peaks']:
                    todo.append((int(round(e3 * muls['peaks'])), 'peaks'))

                for j, e3 in enumerate(e2[1]['bandsums']):
                    todo.append((int(round(e3 * muls['bandsums'])), 
                        'band_{0}'.format(j + 1)))

                for e3 in todo:
                    if e3[0] not in result[e3[1]]:
                        result[e3[1]][e3[0]] = []
                    result[e3[1]][e3[0]].append((k, i, e2[0]))
    return result
        
def listfiles(dr):
    return [f for f in os.listdir(dr) if os.path.isfile(os.path.join(dr, f))]

def offline_process(fn, nt, slide = 0):
    c1, c2, SR = readwave(fn)

    n = int(SR * nt)

    mx = len(c1)
    numchunks = mx // n
    chunks = []
    if slide == 0:
        slide = nt

    for i in range(numchunks):
        chk = c1[i * n: (i + 1) * n]
        chunks.append(chk)

    vols =  [volume_t(e) for e in chunks]
    maxloc = np.argmax(vols)
    #ok, we now have the max

    if (maxloc + measloc[0] < 0) or (maxloc + measloc[-1] >= len(vols)):
        return {'err': 1}

    rel_chunks = [np.array(chunks[maxloc + i], dtype='float64') 
        for i in measloc]

#    print [volume_t(x) for x in rel_chunks]
    res = {'fn': fn[fn.rfind('/') + 1:]}

#    ffts = [smoothen(np.fft.fft(x), 2) for x in rel_chunks]
    ffts = [np.fft.fft(x) for x in rel_chunks]
    
    fft_e_content(res, ffts, '1', 100, 500, SR, n)

    E_center(res, SR, n, ffts, smooth = 2)
    
    return res
  

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Usage: mode {0} dir".format(sys.argv[0])
        print "\n\tmode: 0 for building db, 1 for matching file"
        quit()

    if sys.argv[1] == '0':
        prefix = sys.argv[2] + '/'        
        inps = listfiles(prefix)

        db = {}
        dbl = []
        for e in inps:
            if (e.find('aut') == -1) and (e.find('bus') == -1):
                continue

            off = offline_process(prefix + e, FFT_TIMELEN, slide = FFT_SLIDE)
            if 'err' in off:
                continue
            dbl.append(off)                
#            print "E", e
#            process_file(db, prefix + e, FFT_TIMELEN, FFT_SLIDE)


        keys = set(dbl[0].keys())
        keys.remove('fn')
        keys = sorted(keys)


        tallymap = {}
        for e in keys:
            classa = [xx[e] for xx in dbl if xx['fn'].find('aut') != -1]
            classb = [xx[e] for xx in dbl if xx['fn'].find('bus') != -1]

            ldct = {}
            ldct['a'] = {'E': np.average(classa), 'std': np.std(classa)}
            ldct['b'] = {'E': np.average(classb), 'std': np.std(classb)}
            tallymap[e] = ldct

        print tallymap
        quit()

        dbstats = make_db_stats(db)

        f = open('chunkdb.py', 'wt')
        f.write('chunkdb={0}\n'.format(db))
        f.write('dbstats={0}'.format(dbstats))
        f.close()
        quit()

    #now we match
    db0 = {}
    from chunkdb import chunkdb, dbstats
    process_file(db0, sys.argv[2], FFT_TIMELEN, FFT_SLIDE)

    cands = {'relpeaks': {}, 'peaks': {}, 'bandsums': {}}
    timeblocks = set([])
    lastvol = 0
    active = 0

    for i, e in enumerate(db0[min(db0)][0]):
        print "TIME=", i
        if (i in timeblocks) or(e[1]['volume'] < VOLUME_THR):
            print "FAIL0"
            lastvol = 0
            active = 0
            continue
        
        vol = e[1]['vol_filtfilt']
        if vol > lastvol: #turn off at bottom
            if active == 1:
                active = 0
        else:
            if active == 0:
                active = 1

        lastvol = vol
        if not active:
            print "OFF-peak"
            continue

        tll = lookup_chunk(e, chunkdb, dbstats)

        if ('-' in tll['maxpeak']) or ('-' in tll['Eavg']):
            print "FAIL"
            continue

        enter_candidates_by_key('relpeaks', i, cands, tll)
        enter_candidates_by_key('peaks', i, cands, tll)
        enter_candidates_by_key('bandsums', i, cands, tll)

        winners = flush_candidates(i, cands, FLUSH_AFTER, HIT_MIN_LEN,
            HIT_MIN_THR, PROB_THR)

        bestwinner = None
        bestsc = 0
        for k, v in winners.items():
            if v > bestsc:
                bestwinner = k
                bestsc = v

        if bestwinner:
            print "WINNERS:", bestwinner, bestsc, i
            cands = {'relpeaks': {}, 'peaks': {}, 'bandsums': {}}
            timeblocks.add(i + 1)
            timeblocks.add(i + 2)
            timeblocks.add(i + 3)

        print cands
