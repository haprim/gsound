import numpy as np

def volume_t(chunk):
    return np.average(np.abs(chunk))

def voldet(res, chunklst):
    vols = [volume_t(x) for x in chunklst]
    maxvoll = np.argmax(vols)
    mv = float(vols[maxvoll])

    relvols = [e / mv for e in vols]

    res['voldet'] = np.average(relvols)
    res['voldet_std'] = np.std(relvols)
    res['maxabsvol'] = mv

    return 

def fft_e_content(res, lspectrum, tag, fr1, fr2, SR, n):
    reso = SR / float(n)
    bin1 = int(fr1 / reso)
    bin2 = 1 + int(fr2 / reso)
    octp3 = 2 ** (0.25)

    counter = []
    for spec in lspectrum:
        f00 = 20

        sumb1 = 0
        sumb2 = 0
        spcsum = []

        while True:
            if f00 < fr1:
                f00 *= octp3
                continue
            elif f00 * octp3 < fr1:
                f00 *= octp3
                continue

            if f00 > fr2:
                break

            bin1 = int(f00 / reso)
            bin2 = int(f00 * octp3 / reso)
#            print bin1, bin2, spec[bin1:bin2]

#            dbval = np.log10(np.average(np.abs(spec[bin1:bin2])))
#            dbval = np.log10(np.average(np.abs(spec[bin1:bin2])))
#            dbval = np.log10(np.average(np.abs(spec[bin1:bin2])))
            rewo1 = np.abs(spec[bin1:bin2])**2
            rewo2 = np.sqrt(sum(rewo1) / (bin2 - bin1))

            dbval = np.log10(rewo2)
#            dbval = np.log10(np.average(np.abs(spec[bin1:bin2])))
            f00 *= octp3
            spcsum.append(dbval)

        counter.append(sum(spcsum))
#        print counter
#        quit()

#    print counter
    res['ffteco' + tag] = np.average(counter)       
    res['ffteco' + tag + '_std'] = np.std(counter)
    return 

def fit_line_on_volumes(res, vols):
    mx = np.max(vols)
    v2 = [x / float(mx) for x in vols]
    aa = np.polyfit([i for i in range(len(vols))], vols, 1)
    res['voldecay'] = aa[0]
    return

def smoothen(ch, sz):
    if sz == 0:
        return ch

    result = []
    for i in range(sz, len(ch) - sz):
        result.append(np.average(ch[i - sz: i + sz + 1]))
    return np.array(result)        


def E_center(res, SR, n, chunks, smooth = 0):
    tmpl = []
    reso = SR / float(n)
    fr1 = 100
    fr2 = 2500

    bin1 = int(fr1 / reso)
    bin2 = int(fr2 / reso)
    for e0 in chunks:
        e = e0[bin1: bin2]
        if smooth > 0:
            e = smoothen(e, smooth)
        bse = [np.log(x + 2) for x in range(len(e))]
        avg = np.average(bse, weights = np.abs(e))
        tmpl.append(avg)

    res['Ecenter'] = np.average(tmpl)
    res['Ecenter_std'] = np.std(tmpl)
    rr = {}
    fit_line_on_volumes(rr, tmpl)
    res['ECslope'] = rr['voldecay']
    return

   
