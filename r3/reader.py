import os
import sys
import scipy.io.wavfile as wf
import scipy.signal as scs
import matplotlib.pyplot as plt
import numpy as np

FFT_TIMELEN = 0.1
FFT_SLIDE = 0
FREQ_CUTOFF = (50, 2000)
NUMPEAKS = 7
MIN_PEAK_DIST = 4
LOGBASE = 2 ** (1/12.0)
PKTHR1 = 2
FLUSH_AFTER = 2
HIT_MIN_LEN = 8
HIT_MIN_THR = 0.7
PROB_THR = 0.5
VOLUME_THR = 3000
SMOOTH_SIZE = 2
INTERVALS = [100, 200, 400, 800, 1300]
BAND_SEEK_SIZE = 1

muls = {'relpeaks': 0.5, 'peaks': 0.5, 'maxpeak': 0.5, 'Eavg': 10, 
    'bandsums': 100}
default_keys = ['a', 't', 'b', 'v']

def readwave(fn):
    rate, stream = wf.read(fn)
    data0 = stream[:, 0]
    data1 = stream[:, 1]
    assert(len(data0) == len(data1))

    return data0, data1, rate

def stats(fftseg, n, binrange, SR):
    freqres = SR / float(n)
    
    intervals = []
    for e in INTERVALS:
        binno = int(round(e / freqres))
        if len(intervals) == 0:
            intervals.append((int(round(binrange[0])), binno))
        else:
            intervals.append((intervals[-1][1], binno))
    intervals.append((intervals[-1][1], int(round(binrange[1]))))

    xbase = [np.log(x + 2) for x in range(len(fftseg))]
    avg = np.average(xbase[binrange[0]:], weights = fftseg[binrange[0]:])
#    avg = np.average(xbase, weights = fftseg)

#    bandavgs = [np.log(e[0] + 2) + np.average(xbase[e[0]: e[1]], 
    bandavgs = [np.average(xbase[e[0]: e[1]], 
        weights = fftseg[e[0]: e[1]]) for e in intervals]

    bandsums = [np.sum(fftseg[e[0]: e[1]]) for e in intervals]
    bsnorm = float(sum(bandsums))

#    bandsums = [int(round(muls['bandsums'] * x / bsnorm)) for x in bandsums]
    bandsums = [x / bsnorm for x in bandsums]

#    print "peak", avg
    
    lnm1 = len(fftseg) - 1

    maxima = []
    import math
    for i, e in enumerate(fftseg):
        if (i == 0) or (i == lnm1):
            continue
        if i < binrange[0]:
            continue

        if (e > fftseg[i - 1]) and (e > fftseg[i + 1]):
            maxima.append((e, math.log(i+1, LOGBASE)))

    maxima = list(reversed(sorted(maxima)))

    maxpeak = maxima[0][1]
    peakset = set([])
    relpeakset = []

    ctr = NUMPEAKS
    for e in maxima:
        if any(abs(x - e[1]) < MIN_PEAK_DIST for x in peakset):
            continue

        peakset.add(e[1])
        relpeakset.append(int(round(e[1] - maxpeak)))

        ctr -= 1
        if ctr == 0:
            break

#    relpeakset.pop(0)
    result = {}
    result['Eavg'] = avg
    result['peaks'] = [int(round(x)) for x in sorted(peakset)]
    result['relpeaks'] = [x for x in sorted(relpeakset) if x != 0]
    result['maxpeak'] = maxpeak
    result['bandavgs'] = bandavgs
    result['bandsums'] = bandsums

#    print "RES+", result
    return result


#    print list(reversed(sorted(maxima)))[:NUMPEAKS]


def make_db_stats(db):
    result = {'relpeaks': {}, 'peaks': {}, 'Eavg': {}, 'maxpeak': {}}
    for i in range(len(INTERVALS) + 1):
        result['band_{0}'.format(i + 1)] = {}

    for k, v in db.items():
        for i, e in enumerate(v):
            for e2 in e:
                todo = []
                print "e2=", e2
                mp2 = int(round(e2[1]['maxpeak'] * muls['maxpeak']))
                todo.append((mp2, 'maxpeak'))

                eavg = int(round(e2[1]['Eavg'] * muls['Eavg']))
                todo.append((eavg, 'Eavg'))

                for e3 in e2[1]['relpeaks']:
                    todo.append((int(round(e3 * muls['relpeaks'])), 'relpeaks'))

                for e3 in e2[1]['peaks']:
                    todo.append((int(round(e3 * muls['peaks'])), 'peaks'))

                for j, e3 in enumerate(e2[1]['bandsums']):
                    todo.append((int(round(e3 * muls['bandsums'])), 
                        'band_{0}'.format(j + 1)))

                for e3 in todo:
                    if e3[0] not in result[e3[1]]:
                        result[e3[1]][e3[0]] = []
                    result[e3[1]][e3[0]].append((k, i, e2[0]))
    return result
        
#def add_to_db(db, 
def intensity(ch, SR):
    erate = 10.0

    rect = np.abs(ch)
    b, a = scs.butter(1, erate / (SR / 2.0))
    env = scs.filtfilt(b, a, rect, padlen = 3)
    return np.average(env)

def process_channel(SR, ch, index, n, binrange, smoothsize = 0, slide = 0):
    sl = slide
    if sl == 0:
        sl = n

    chtmp = ch[index: index + n]

    intns = intensity(ch, SR)

    ffr = np.abs(np.fft.fft(chtmp))
#    plt.plot(ffr[: binrange[1] + 1])
#    plt.show()
    useful = ffr[:binrange[1] + 1]
    useful = smoothen(useful, smoothsize)
#    plt.plot(useful)
#    plt.show()
#    quit()

    pst = stats(useful, n, binrange, SR)
    pst['volume'] = np.average(np.abs(ffr))
    pst['vol_filtfilt'] = intns
    print pst['Eavg']
    return pst

def smoothen(ch, sz):
    if sz == 0:
        return ch

    result = []
    for i in range(sz, len(ch) - sz):
        result.append(np.average(ch[i - sz: i + sz + 1]))
    return np.array(result)        


def process_file(db, fn, nt, slide):
    c1, c2, SR = readwave(fn)

    n = int(SR * nt)

    c0 = (c1 + c2) / 2

    mx = len(c0)

    ctr = 0
    singctr = 0

    binrange = ((FREQ_CUTOFF[0] * n) / SR, (FREQ_CUTOFF[1] * n) / SR)
    cls = fn[fn.rfind('/') + 1]
    if cls not in db:
        db[cls] = []

#    print binrange, SR

    loc = []

    while True:
        if ctr >= mx - n:
            break

#        if singctr < 20:
#            continue
        chunk_stats = process_channel(SR, c0, ctr, n, binrange, SMOOTH_SIZE, 
            slide)

        loc.append((singctr, chunk_stats))
        ctr += n
        singctr += 1

    db[cls].append(loc)

def listfiles(dr):
    return [f for f in os.listdir(dr) if os.path.isfile(os.path.join(dr, f))]

def lookup_chunk(cxr0, chunkdb, dbstats): #chunk_extract
    votes = {'maxpeak': None, 'relpeaks': None, 'peaks': None, 'Eavg': None,
        'bandsums': None}
    tallyho = {}

    #maxpeak
    cxr = cxr0[1]
    mp2 = int(round(cxr['maxpeak'] * muls['maxpeak']))
    lt = {'-' : 1.0}
    if mp2 in dbstats['maxpeak']:
        lt = {x: 2 for x in default_keys}
        for e in dbstats['maxpeak'][mp2]:
            lt[e[0]] += 1
    sm = sum(lt.values())            
    lt = {k: v / float(sm) for k, v in lt.items()}
    tallyho['maxpeak'] = lt

    lt = {'-' : 1.0}
    eavg = int(round(cxr['Eavg'] * muls['Eavg']))
    if eavg in dbstats['Eavg']:
        lt = {x: 2 for x in default_keys}
        for e in dbstats['Eavg'][eavg]:
            lt[e[0]] += 1
    sm = sum(lt.values())            
    lt = {k: v / float(sm) for k, v in lt.items()}
    tallyho['Eavg'] = lt

    lt = {}
    mem = set([])
    for i, e0 in enumerate(cxr['relpeaks']):
        e = int(round(e0 * muls['relpeaks']))
        if e in dbstats['relpeaks']:
            for e2 in dbstats['relpeaks'][e]:
                key = '{0}_{1}_{2}'.format(e2[0], e2[1], e2[2])
                key0 = key + '_{0}'.format(i)
                if key0 in mem:
                    continue
                mem.add(key0)

                if key not in lt:
                    lt[key] = 0

                lt[key] += 1
    lt2 = {k: v / float(NUMPEAKS - 1) for k, v in lt.items() if v > PKTHR1}
    tallyho['relpeaks'] = lt2

    lt = {}
    mem = set([])
    for i, e0 in enumerate(cxr['peaks']):
        e = int(round(e0 * muls['peaks']))
        if e in dbstats['peaks']:
            for e2 in dbstats['peaks'][e]:
                key = '{0}_{1}_{2}'.format(e2[0], e2[1], e2[2])
                key0 = key + '_{0}'.format(i)
                if key0 in mem:
                    continue
                mem.add(key0)

                if key not in lt:
                    lt[key] = 0

                lt[key] += 1
    lt2 = {k: v / float(NUMPEAKS) for k, v in lt.items() if v > PKTHR1}                
    tallyho['peaks'] = lt2


    lt = {}
    mem = set([])
    bandcands = set([])
    for i, e0 in enumerate(cxr['bandsums']):
        tempcands = set([])
        varkey = 'band_{0}'.format(i + 1)
        e = int(round(e0 * muls['bandsums']))
        bss = int(max(e * 0.3, BAND_SEEK_SIZE))
        bss = int(max(e * 0.1, BAND_SEEK_SIZE))

        seek = range(e - bss, e + bss + 1)

        for e2 in seek:
            if e2 in dbstats[varkey]:
                for e3 in dbstats[varkey][e2]:
                    tempcands.add((e3[0], e3[1], e3[2]))
        if i == 0:
            bandcands = tempcands
            continue

        bandcands = bandcands.intersection(tempcands)            
#        print "BANDCANDS", bandcands
#    print "FIN:", bandcands

    tallyho['bandsums'] = {"{0}_{1}_{2}".format(x[0], x[1], x[2]): 1 for x
        in bandcands}

#    print tallyho

    return tallyho

def enter_candidates_by_key(main_key, tm, cands, tll):
    #first check the old ones
    blocks = set([])
    for k, v in cands[main_key].items():
        for e in v:
            cur_seg = tm - e['start_at'] + e['start_seg']
            searchkey = k + "_{0}".format(cur_seg)

            if searchkey in tll[main_key]:
                e['last_seen'] = tm
                e['prob'] *= tll[main_key][searchkey]
                e['nhits'] += 1

                blocks.add(searchkey)

    for k, v in tll[main_key].items():            
        if k in blocks:
            continue
        sepped = k.split('_')            
        nk = '{0}_{1}'.format(sepped[0], sepped[1])

        dct = {'start_at': tm, 'start_seg': int(sepped[2]), 'last_seen': tm,
            'prob': v, 'nhits': 1}
        if nk not in cands[main_key]:            
            cands[main_key][nk] = [dct]
        else:
            cands[main_key][nk].append(dct)
        
def flush_candidates(tm, cands, flush_thr, hit_len, hit_thr, prob_thr):
    kills = []
    min_time = tm - FLUSH_AFTER + 1

    #kills
    for k, v in cands.items():
        if k == 'relpeaks':
            continue
        for vehicle, lst in v.items():
            copylst = []
            for e in lst:
                if e['last_seen'] >= min_time:
                    copylst.append(e)
            v[vehicle] = copylst

    #wins
    winners = {}
    for k, v in cands.items():
        for vehicle, lst in v.items():
            for e in lst:
                interval = e['last_seen'] - e['start_at']
                if (interval >= hit_len) and (e['nhits'] / float(interval) >=
                    hit_thr) and (e['prob'] >= prob_thr ** e['nhits']):
                    winners[vehicle] = e['prob']

    return winners                    

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Usage: mode {0} dir".format(sys.argv[0])
        print "\n\tmode: 0 for building db, 1 for matching file"
        quit()

    if sys.argv[1] == '0':
        prefix = sys.argv[2] + '/'        
        inps = listfiles(prefix)

        db = {}
        for e in inps:
            print "E", e
            process_file(db, prefix + e, FFT_TIMELEN, FFT_SLIDE)

        dbstats = make_db_stats(db)

        f = open('chunkdb.py', 'wt')
        f.write('chunkdb={0}\n'.format(db))
        f.write('dbstats={0}'.format(dbstats))
        f.close()
        quit()

    #now we match
    db0 = {}
    from chunkdb import chunkdb, dbstats
    process_file(db0, sys.argv[2], FFT_TIMELEN, FFT_SLIDE)

    cands = {'relpeaks': {}, 'peaks': {}, 'bandsums': {}}
    timeblocks = set([])
    lastvol = 0
    active = 0

    for i, e in enumerate(db0[min(db0)][0]):
        print "TIME=", i
        if (i in timeblocks) or(e[1]['volume'] < VOLUME_THR):
            print "FAIL0"
            lastvol = 0
            active = 0
            continue
        
        vol = e[1]['vol_filtfilt']
        if vol > lastvol: #turn off at bottom
            if active == 1:
                active = 0
        else:
            if active == 0:
                active = 1

        lastvol = vol
        if not active:
            print "OFF-peak"
            continue

        tll = lookup_chunk(e, chunkdb, dbstats)

        if ('-' in tll['maxpeak']) or ('-' in tll['Eavg']):
            print "FAIL"
            continue

        enter_candidates_by_key('relpeaks', i, cands, tll)
        enter_candidates_by_key('peaks', i, cands, tll)
        enter_candidates_by_key('bandsums', i, cands, tll)

        winners = flush_candidates(i, cands, FLUSH_AFTER, HIT_MIN_LEN,
            HIT_MIN_THR, PROB_THR)

        bestwinner = None
        bestsc = 0
        for k, v in winners.items():
            if v > bestsc:
                bestwinner = k
                bestsc = v

        if bestwinner:
            print "WINNERS:", bestwinner, bestsc, i
            cands = {'relpeaks': {}, 'peaks': {}, 'bandsums': {}}
            timeblocks.add(i + 1)
            timeblocks.add(i + 2)
            timeblocks.add(i + 3)

        print cands
