import os
import sys
import scipy.io.wavfile as wf
import scipy.signal as scs
import matplotlib.pyplot as plt
import numpy as np

from worker import *
from learndata import tallymap
import scipy.stats as sst

measloc = range(-3, 3)
if (measloc[0] > 0) or (measloc[-1] < 0):
    assert(0)

zeroindex = -measloc[0]    

filterkeys = ['ECslope', 'voldet', 'maxabsvol', 'ffteco1', 'Ecenter']
numsigma_filter = 3
num_can_err = 1

#matchkeys = ['ffteco1', 'Ecenter', 'maxabsvol']
matchkeys = ['ffteco1', 'Ecenter']
numsigma_match = 2

FFT_TIMELEN = 0.1
FFT_SLIDE = 0
FREQ_CUTOFF = (50, 2000)
NUMPEAKS = 7
MIN_PEAK_DIST = 4
LOGBASE = 2 ** (1/12.0)
PKTHR1 = 2
FLUSH_AFTER = 2
HIT_MIN_LEN = 8
HIT_MIN_THR = 0.7
PROB_THR = 0.5
VOLUME_THR = 3000
SMOOTH_SIZE = 2
INTERVALS = [100, 200, 400, 800, 1300]
BAND_SEEK_SIZE = 1

def readwave(fn):
    rate, stream = wf.read(fn)
    data0 = stream[:, 0]
    data1 = stream[:, 1]
    assert(len(data0) == len(data1))

    return data0, data1, rate


def listfiles(dr):
    return [f for f in os.listdir(dr) if os.path.isfile(os.path.join(dr, f))]

def calcstats(fn, raw_chunks, SR, n):
    res = {'fn': fn}

    voldet(res, raw_chunks)

#    ffts = [smoothen(np.fft.fft(x), 2) for x in rel_chunks]
    ffts = [np.fft.fft(x) for x in raw_chunks]
    
    fft_e_content(res, ffts, '1', 100, 500, SR, n)

    E_center(res, SR, n, ffts, smooth = 2)
    return res


def offline_process(fn, nt, slide = 0):
    c1, c2, SR = readwave(fn)

    n = int(SR * nt)

    mx = len(c1)
    numchunks = mx // n
    chunks = []
    if slide == 0:
        slide = nt

    for i in range(numchunks):
        chk = c1[i * n: (i + 1) * n]
        chunks.append(chk)

    vols =  [volume_t(e) for e in chunks]
    maxloc = np.argmax(vols)
    #ok, we now have the max

    if (maxloc + measloc[0] < 0) or (maxloc + measloc[-1] >= len(vols)):
        return {'err': 1}

    rel_chunks = [np.array(chunks[maxloc + i], dtype='float64') 
        for i in measloc]

    res = calcstats(fn[fn.rfind('/') + 1:], rel_chunks, SR, n)
#    print [volume_t(x) for x in rel_chunks]
    return res
 

def online_process(fn, nt, slide = 0):
    c1, c2, SR = readwave(fn)
    results = []

    n = int(SR * nt)

    mx = len(c1)
    numchunks = mx // n
    active_chunks = []
    volumes = []
    if slide == 0:
        slide = nt

    ctr = 0
    lmeas = len(measloc)

    tmm = tallymap['maxabsvol']
    minabsvol = min(tmm['auto']['E'] - numsigma_match * tmm['auto']['std'],
        tmm['bus']['E'] - numsigma_match * tmm['bus']['std'])

    while ctr < numchunks:
        if ctr % 1000 == 0:
            print ctr

        chk = c1[ctr * n: (ctr + 1) * n]
        if ctr >= lmeas:
            active_chunks.pop(0)
            volumes.pop(0)

        active_chunks.append(chk)
        volumes.append(volume_t(chk))

        maxvolloc = np.argmax(volumes)
        if maxvolloc != zeroindex:
            ctr += 1
            continue

        vv = volumes[maxvolloc]
        if vv < minabsvol:
#            print "TOO QUIET", ctr
            ctr += 1
            continue
#        print "MAX AT", ctr                

        stats = calcstats('t{0}'.format(ctr), active_chunks, SR, n)
        cands = set(['auto', 'bus'])

#        print stats
        ### FILTER
        for e1 in ['auto', 'bus']:
            numerr = 0
            for e2 in filterkeys:
                mean = tallymap[e2][e1]['E']
                std = tallymap[e2][e1]['std']
                val = stats[e2]
                if (val >= mean - numsigma_filter * std) and \
                        (val <= mean + numsigma_filter * std):
                    pass
#                    print "--- CAN BE", e1, e2
                else:
#                    print "XXX CANNOT BE", e1, e2, val, mean, std
                    numerr += 1
                    if numerr > num_can_err:
                        cands.remove(e1)
                        break

        if len(cands) == 0:
            continue

        ### if we are here, that means we have seen a vehicle
        if len(cands) == 1:
            results.append((ctr * FFT_TIMELEN, {min(cands): 1.0}))
            continue

        relps = {}
        for e1 in cands:
            locval = 0.0
            for e2 in matchkeys:
                mean = tallymap[e2][e1]['E']
                std = tallymap[e2][e1]['std']
                val = stats[e2]
                stded = (val - mean) / std
                if stded > 0:
                    stded = -stded

                pp = sst.norm.cdf(stded) / 2.0
                locval += np.log(pp)
            relps[e1] = locval / len(matchkeys)                

        for k in relps.keys():
            relps[k] = np.exp(relps[k])

        smfac = sum(relps.values())            
        for k in relps.keys():
            relps[k] /= smfac

        results.append((ctr * FFT_TIMELEN, relps))

        ctr += 1

    for e in results:
        print "AT {0}: {1}".format("%.2f" % e[0], e[1])
    quit()        

    vols =  [volume_t(e) for e in chunks]
    maxloc = np.argmax(vols)
    #ok, we now have the max

    if (maxloc + measloc[0] < 0) or (maxloc + measloc[-1] >= len(vols)):
        return {'err': 1}

    rel_chunks = [np.array(chunks[maxloc + i], dtype='float64') 
        for i in measloc]

#    print [volume_t(x) for x in rel_chunks]
    res = {'fn': fn[fn.rfind('/') + 1:]}

#    ffts = [smoothen(np.fft.fft(x), 2) for x in rel_chunks]
    ffts = [np.fft.fft(x) for x in rel_chunks]
    
    fft_e_content(res, ffts, '1', 100, 500, SR, n)

    E_center(res, SR, n, ffts, smooth = 2)
    
    return res
 

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Usage: mode {0} dir".format(sys.argv[0])
        print "\n\tmode: 0 for building db, 1 for matching file"
        quit()

    if sys.argv[1] == '0':
        prefix = sys.argv[2] + '/'        
        inps = listfiles(prefix)

        db = {}
        dbl = []
        for e in inps:
            if (e.find('aut') == -1) and (e.find('bus') == -1):
                continue

            off = offline_process(prefix + e, FFT_TIMELEN, slide = FFT_SLIDE)
            if 'err' in off:
                continue
            dbl.append(off)                
#            print "E", e
#            process_file(db, prefix + e, FFT_TIMELEN, FFT_SLIDE)


        keys = set(dbl[0].keys())
        keys.remove('fn')
        keys = sorted(keys)


        tallymap = {}
        for e in keys:
            classa = [xx[e] for xx in dbl if xx['fn'].find('aut') != -1]
            classb = [xx[e] for xx in dbl if xx['fn'].find('bus') != -1]

            ldct = {}
            ldct['auto'] = {'E': np.average(classa), 'std': np.std(classa)}
            ldct['bus'] = {'E': np.average(classb), 'std': np.std(classb)}
            tallymap[e] = ldct

        print tallymap

#        dbstats = make_db_stats(db)

        f = open('learndata.py', 'wt')
        f.write('tallymap={0}\n'.format(tallymap))
        f.close()
        quit()

    #now we match
    db0 = {}

    online_process(sys.argv[2], FFT_TIMELEN)
    quit()


    process_file(db0, sys.argv[2], FFT_TIMELEN, FFT_SLIDE)

    cands = {'relpeaks': {}, 'peaks': {}, 'bandsums': {}}
    timeblocks = set([])
    lastvol = 0
    active = 0

    for i, e in enumerate(db0[min(db0)][0]):
        print "TIME=", i
        if (i in timeblocks) or(e[1]['volume'] < VOLUME_THR):
            print "FAIL0"
            lastvol = 0
            active = 0
            continue
        
        vol = e[1]['vol_filtfilt']
        if vol > lastvol: #turn off at bottom
            if active == 1:
                active = 0
        else:
            if active == 0:
                active = 1

        lastvol = vol
        if not active:
            print "OFF-peak"
            continue

        tll = lookup_chunk(e, chunkdb, dbstats)

        if ('-' in tll['maxpeak']) or ('-' in tll['Eavg']):
            print "FAIL"
            continue

        enter_candidates_by_key('relpeaks', i, cands, tll)
        enter_candidates_by_key('peaks', i, cands, tll)
        enter_candidates_by_key('bandsums', i, cands, tll)

        winners = flush_candidates(i, cands, FLUSH_AFTER, HIT_MIN_LEN,
            HIT_MIN_THR, PROB_THR)

        bestwinner = None
        bestsc = 0
        for k, v in winners.items():
            if v > bestsc:
                bestwinner = k
                bestsc = v

        if bestwinner:
            print "WINNERS:", bestwinner, bestsc, i
            cands = {'relpeaks': {}, 'peaks': {}, 'bandsums': {}}
            timeblocks.add(i + 1)
            timeblocks.add(i + 2)
            timeblocks.add(i + 3)

        print cands
