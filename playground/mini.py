import sys
import math

deg = float(sys.argv[1])
dist = float(sys.argv[2])

x0 = 2.5

phi = deg * math.pi / 180.0

ht = dist * math.sin(phi)

x1 = dist * math.cos(phi) - x0
x2 = x1 + 2 * x0

nx2 = (ht**2 + x2**2)**0.5
nx1 = (ht**2 + x1**2)**0.5
diff = nx2 - nx1
print "diff=", diff, nx1, nx2, x1, x2, ht
