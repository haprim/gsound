import wave
import numpy as np
import sys
import scipy.io.wavfile as wf
import scipy.signal as scs
import scipy.stats
import matplotlib.pyplot as plt
import cmath
import math
from snd_math import *


if __name__ == "__main__":
#    fn = sys.argv[1]
    path = sys.argv[1]

    filenames = ['0deg', 'm30deg', 'm60deg', 'm90deg', 'm120deg', 'm150deg',
        '180deg', 'neardoor1', 'nextroom1', 'nextroom2', 'close1_m120',
        'close2_m75', 'close3_m30']

#    filenames = ['0deg', 'm30deg', 'm60deg', 'm90deg', 'm120deg', 'm150deg',
#        '180deg']
        
    filenames = ['nextroom1', 'nextroom2']

    filenames = ['0', '30', '60', '90', '120', '140',
        '150', '180',
        'bathroom', 'd60',  'nextroom1', 'close_above_45',
        'd90', 'nextroom2', 'above135',
        'd135', 'doorway', 'rightabove']

    filenames = ['0f1', '0f2']
    filenames = ['hatter1', 'hatter2', 'hatter3', 'hatter4']

#    filenames = ['nextroom1']        

    result = {}

#    path='../data/'
#    filenames=['puretone']

    for e in filenames:
        fn = path + '/' + e + '.wav'
        c1, c2, SR = readwave(fn)
#        stereo_fft(c1, c2, 4096, 512, result)
#        stereo_fft(c1, c2, 1024, 8192, 256, result)
#        stereo_fft(c1, c2, 4096, 32768, 2048, result)
#        stereo_fft(c1, c2, 1323, 32768, 1000, result)
        ctr = 0
        n = 4096
        step = 4096
        pads = 32000
        while True:
            rr = stereo_fft(c1, c2, ctr, n, pads, 4, ctr, result)
            ctr += step
            if ctr + n >= len(c1):
                break
#        stereo_fft(c1, c2, 32768, 32768, 25600, result)

    print "known_backgrounds=", result

