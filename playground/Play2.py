import os
import wave
import numpy as np
import sys
import scipy.io.wavfile as wf
from scipy.io.wavfile import write, read
from scipy.signal import get_window, resample, blackmanharris, triang
from scipy.fftpack import fft, ifft, fftshift
import matplotlib.pyplot as plt
import cmath
import math, copy, sys, os

tol = 1e-5  

def isPower2(num):
	return ((num & (num - 1)) == 0) and num > 0

#Take as N a power of 2
def dftA(x, w, N):
	hN = (N/2)+1                                             
	hM1 = int(math.floor((w.size+1)/2))                      
	hM2 = int(math.floor(w.size/2))                          
	fftbuffer = np.zeros(N)                                  
	w = w / sum(w)                                           
	xw = x*w                                                 
	fftbuffer[:hM1] = xw[hM2:]                               
	fftbuffer[N-hM2:] = xw[:hM2]        
	X = fft(fftbuffer)                                       
	absX = abs(X[:hN])                                       
	absX[absX<np.finfo(float).eps] = np.finfo(float).eps     
	mX = 20 * np.log10(absX)                                 
	X[:hN].real[np.abs(X[:hN].real) < tol] = 0.0             
	X[:hN].imag[np.abs(X[:hN].imag) < tol] = 0.0                     
	pX = np.unwrap(np.angle(X[:hN]))                         
	return mX, pX


#Take hop size H positive
def stftA(x, fs, w, N, H) :
	M = w.size                                       
	hM1 = int(math.floor((M+1)/2))                  
	hM2 = int(math.floor(M/2))                       
	x = np.append(np.zeros(hM2),x)                   
	x = np.append(x,np.zeros(hM2))                   
	pin = hM1                                               
	pend = x.size-hM1                                
	w = w / sum(w)                                   
	y = np.zeros(x.size)                             
	while pin<=pend:                                   
		x1 = x[pin-hM1:pin+hM2]                        
		mX, pX = dftA(x1, w, N)                 
		if pin == hM1:                                 
			xmX = np.array([mX])
			xpX = np.array([pX])
		else:                                          
			xmX = np.vstack((xmX,np.array([mX])))
			xpX = np.vstack((xpX,np.array([pX])))
		pin += H                                       
	return xmX, xpX


window = 'hamming'

M = 801
N = 1024
H = 400

def readwave(fn):
    rate, stream = wf.read(fn)
    data0 = stream[:, 0]
    data1 = stream[:, 1]
    assert(len(data0) == len(data1))
    return rate, data0, data1

if __name__ == "__main__":
    inputFile = "/home/adxpoi/Downloads/001Soundstuff/m150deg.wav"

    if len(sys.argv) > 1:
        inputFile = sys.argv[1]

    fs, x, xxx = readwave(inputFile)

    w = get_window(window, M)

    mmmX, ppppX = stftA(x, fs, w, N, H)

    plt.pcolormesh(np.transpose(mmmX))
    plt.show()
