import wave
import numpy as np
import sys
import scipy.io.wavfile as wf
import scipy.signal as scs
import matplotlib.pyplot as plt
import cmath
import math

def readwave(fn):
    rate, stream = wf.read(fn)
    data0 = stream[:, 0]
    data1 = stream[:, 1]
    assert(len(data0) == len(data1))

    return data0, data1

def stereo_fft(c1, c2, n, slide, result = {}):
    mx = len(c1)
    ctr = 000
    tally = {}
    window = scs.flattop(n)
#    window = np.array([1 for _ in range(n)])

    binsize = 44100 / float(n)
    desbins = range(50, 1600, 50)

#    desbins = [200, 400, 825, 1100, 1650, 2500, 3300, 4950, 6600]
    for i in range(len(desbins)):
        desbins[i] = int(desbins[i] / binsize)

    maxamp = 0

    """
    while True:
        E = sum(np.absolute(c1[ctr:min(ctr + n, mx)]))
        E += sum(np.absolute(c2[ctr:min(ctr + n, mx)]))
        if E > maxamp:
            maxamp = E

        ctr += slide
        if ctr >= mx - n:
            break
    """            


    ctr = 0000
    while True:
        if ctr >= mx - n:
            break

        f1 = np.fft.fft(c1[ctr:min(ctr+n, mx)] * window)
        f2 = np.fft.fft(c2[ctr:min(ctr+n, mx)] * window)

#        print ctr
        E = sum(np.absolute(c1[ctr:min(ctr + n, mx)]))
#        print E / n
        if E / n < 80: ##CUTOFF
            ctr += n
            continue
#        E += sum(np.absolute(c2[ctr:min(ctr + n, mx)]))
#        if E < 0.3 * maxamp:
#            ctr += slide
#            continue
#        plt.plot(f1)
#        plt.plot(f2)
        plottie = []
        for x in desbins:
            phd = cmath.phase(f1[x]) - cmath.phase(f2[x])
            if phd >= math.pi:
                phd -= 2*math.pi
            elif phd < -math.pi:
                phd += 2*math.pi
            plottie.append(phd)                
            if x not in tally:
                tally[x] = []
            tally[x].append(100*phd / x)
#            tally[x].append(phd)
#        plt.plot(plottie)
#        for e in range(20, 500):
#            if e not in tally:
#                tally[e] = []
#            tally[e].append(cmath.phase(f1[x]                
#        plt.show()
#        print cmath.phase(f1[ind])
#        print cmath.phase(f2[ind])
#        plt.show()
        ctr += slide

    ml = 0
    mv = 100
    ttt = []
    for k in sorted(tally.keys()):
        if k > 3000:
            continue
        v = tally[k]
        ss = np.std(v)
        mm = np.mean(v)
        if ss < mv:
            mv = ss
            ml = k
        print k, mm, ss
        ttt.append((mm, ss, k))
        print "\t{0}: {1}, {2}".format(k, mm, ss)

    print "min:", ml, mv        
#    print tally.values()
    avgpd = np.average([x[0] for x in ttt])
    print "\nAVGM", avgpd
    print "S2", np.average([x[1] for x in ttt])
    for e in ttt:
        if e[2] not in result:
            result[e[2]] = []
        result[e[2]].append(e[0])
    return result        


if __name__ == "__main__":
#    fn = sys.argv[1]
    path = sys.argv[1]

    filenames = ['0deg', 'm30deg', 'm60deg', 'm90deg', 'm120deg', 'm150deg',
        '180deg', 'neardoor1', 'nextroom1', 'nextroom2', 'close1_m120',
        'close2_m75', 'close3_m30']

#    filenames = ['0deg', 'm30deg', 'm60deg', 'm90deg', 'm120deg', 'm150deg',
#        '180deg']
        
    filenames = ['nextroom1', 'nextroom2']

    filenames = ['0', '30', '60', '90', '120', '140',
        '150', '180',
        'bathroom', 'd60',  'nextroom1', 'close_above_45',
        'd90', 'nextroom2', 'above135',
        'd135', 'doorway', 'rightabove']

#    filenames = ['nextroom1']        

    result = {}

    for e in filenames:
        fn = path + '/' + e + '.wav'
        c1, c2 = readwave(fn)
#        stereo_fft(c1, c2, 4096, 1024, result)
#        stereo_fft(c1, c2, 8192, 512, result)
        stereo_fft(c1, c2, 1024, 512, result)

    print "\n\n==========\n\n"
    for k in sorted(result.keys()):
        v = result[k]
        print k, v

    for i in range(len(filenames)):
        print "\n\nFN:", filenames[i]
        v = [abs(result[k][i]) for k in result.keys()]
        print i, sum(v)

        x = result.keys()
        y = [result[k][i] for k in x]
        print len(x), len(y), x, y
        plt.scatter(x, y)
#        plt.show()
        print filenames[i], np.mean(y), np.std(y)
#        print np.mean(v), np.std(v)
