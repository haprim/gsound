import wave
import numpy as np
import sys
import scipy.io.wavfile as wf
#import scipy.signal as scs
#import scipy.stats
import matplotlib.pyplot as plt
#import cmath
import math
from snd_math import *

vsound = 340.29

def max_offset(SR, vsound, micdist):
    return micdist * SR / float(vsound)


def amp_impulses(c1, c2, amp_thr, diff_thr, winsize, min_dur_factor):
    assert(len(c1) == len(c2))

    min_duration = int(min_dur_factor * winsize)

    IDB_DEF = -200
    result = []
    ctr = 0
    lastdb = IDB_DEF
    lastdiff = 0
    impulse_start = -1
    last_time = -min_duration
    block_until = 0

    #states: 0 - off, 1 - in impulse
    state = 0 #off

    while True:
#        print ctr
#        E = ((sum((c1[ctr:ctr + winsize] / 256.0**2)**2) + \
#            0*sum((c2[ctr:ctr + winsize] / 256.0**2)**2)) / (4 * len(c1)**2))**0.5
        E1 = 2 * sum((c1[ctr:ctr + winsize] / 131072.0)**2)
        E2 = 2 * sum((c2[ctr:ctr + winsize] / 131072.0)**2)

        #multiply avg by 2/N, i.e. multiply sum by 1/N
        db = 10 * math.log((E1 + E2) / len(c1), 10)

#        print ctr, db, lastdb
        if (db < amp_thr):
            if ctr < block_until:
                ctr += winsize
                continue

#            ...
            if state == 1:
                result.append((impulse_start, lastdb, lastdiff))

            state = 0
            impulse_start = -1
            lastdb = db
            lastdiff = 0
        else:
            if (db - lastdb > 2 * lastdiff):
                state = 1
                impulse_start = ctr
                lastdiff = db - lastdb
                lastdb = db
                block_until = impulse_start + min_duration

            #perhaps comment this out?                
            elif (db - lastdb < 0):
                if state == 1:
                    result.append((impulse_start, lastdb, lastdiff))
                state = 0
                impulse_start = -1
                lastdb = db
                lastdiff = 0

        """
        if (db >= amp_thr) and (db >= IDB_DEF): #
            
#            print ctr, db, lastdb, diff_thr
#            if db - lastdb > diff_thr:
            if db - lastdb > 0:
#                print "\tOK"
                if ctr - last_time >= min_duration:
                    last_time = ctr
                    impulse_start_db = db
                    result.append(ctr)

        lastdb = db
        """
        ctr += winsize
        if ctr + winsize >= len(c1):
            break
    return result 

def max_corr_offset(c1, c2, ind, lc, rng):
    assert(len(c1) == len(c2))
    irange = int(math.ceil(rng))

    maxcoeff = -1
    mxloc = -2 * rng
    for i in range(0, irange + 1):
        a1 = np.array(c1[ind: ind + lc - i])
        a2 = np.array(c2[ind + i: ind + lc])
#        print len(a1), len(a2), ind, ind + i, ind + lc, ind + lc - i
        coeff = np.corrcoef(a1, a2)[0, 1]
        if coeff > maxcoeff:
            maxcoeff = coeff
            mxloc = i

    for i in range(1, irange + 1):
        a1 = np.array(c1[ind + i: ind + lc])
        a2 = np.array(c2[ind: ind + lc - i])
#        print len(a1), len(a2), ind, ind + i, ind + lc, ind + lc - i
        coeff = np.corrcoef(a1, a2)[0, 1]
        if coeff > maxcoeff:
            maxcoeff = coeff
            mxloc = -i

    return maxcoeff, mxloc

def calc_angle(offs, mxoffs):
    rat = offs / mxoffs
    if rat > 1:
        rat = 1.0
    elif rat < -1:
        rat = -1.0

    return math.acos(rat)        

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Usage: {0} fn".format(sys.argv[0])
        quit()
        
    fn = sys.argv[1]

    c1, c2, SR = readwave(fn)
    print type(c1)
    print "FILE:", fn, SR
    micdist = 0.05
    mxoffs = max_offset(SR, vsound, micdist)
    winsize = int(0.5 * mxoffs / micdist)
    impulses = amp_impulses(c1, c2, -80, 5, winsize, 5)
    print "IMPS", impulses

    ress = []
    for e in impulses:
        ind = e[0] + 0.5 * winsize
        db = e[1]
        dbdiff = e[2] / 10.0
        cc, cl = max_corr_offset(c1, c2, ind, winsize, mxoffs)

        p = 0.1 * math.exp(dbdiff)
#        if p > 1:
#            p = 1
        angle = 180.0 * calc_angle(cl, mxoffs) / math.pi
        ress.append((p, angle, cl, ind))
#        ress.append((angle, cl, ind))
#        if e >= 8400:
#            break

#    st = list(reversed(sorted(ress)))
    st = sorted(ress)

    for e in st:
        print "At {0}: {1} {2} {3}".format(e[3], e[2], e[1], e[0])

