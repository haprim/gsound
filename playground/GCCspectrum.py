import wave
import numpy as np
import sys
import scipy
import scipy.io.wavfile as wf
from scipy.io.wavfile import write, read
from scipy.signal import get_window, resample, blackmanharris, triang
from scipy.fftpack import fft, ifft, fftshift
from scipy import signal as sgnl
from scipy.signal import argrelextrema
import matplotlib.pyplot as plt
import cmath
import math, copy, sys, os




def readwave(fn):
    rate, stream = wf.read(fn)
    data0 = stream[:, 0]
    data1 = stream[:, 1]
    assert(len(data0) == len(data1))
    return rate, data0, data1
    


d = .05                                            #microphone separation in meters
tau_grid = np.linspace(-d/343, d/343, 180)



def TDOA(x1, x2, fs, d, tau_grid):
    

   #Time-frequency transform
   wlen = 1024

   X1, X2 = stft_stereo(x1, x2, wlen)
   X1= X1[range(1,len(X1[:,0])),:]
   X2= X2[range(1,len(X2[:,0])),:]

   f = (float(fs)/wlen) *np.arange(1, wlen/2 +1)
   
   c = 343
   alpha = 10*c/(d*fs)
   spec = nonlin_spec(X1,X2, f, alpha, tau_grid)
   spec = np.amax(np.sum(spec,1),axis=1)
   
   peaks, inds = findpeaks(spec, 2);
   tau = tau_grid[inds[np.arange(0,min(len(inds),1))]][0]
   
   angle = (np.arcsin((344*tau)/0.05)*180)/np.pi
   
   return tau, angle
   
   
def nonlin_spec(X1,X2, f, alpha, tau_grid):
    nbin, nframe  = len(X1[:,0]) , len(X1[0,:])
    ngrid = len(tau_grid)

    spec = np.zeros([ngrid, nbin, nframe])

    P = X1 *np.conjugate(X2)
    P = P /abs(P)

    for ind in np.arange(0,ngrid):
      EXP =  (np.tile((np.exp(-2*1j*np.pi*tau_grid[ind]*f)), (nframe,1))).T
      spec[ind] = np.ones([nbin,nframe]) - np.tanh(alpha*np.real(np.sqrt(2-2*np.real(P*EXP))))      

    return spec
#----------------------------------------------------------------------
def stft_stereo(x1, x2, wlen):
   nsampl = x1.size  #sample size

   #Computing STFT coefficients
   #Defining sine window
   win = np.sin(np.pi*(np.arange(.5,wlen-.5+1))/wlen)
   #win = np.ones(wlen)

   # Zero-padding
   nframe = np.ceil((nsampl/float(wlen))*2)
   x1 = np.append(x1,np.zeros((nframe*wlen/2)-nsampl))
   x2 = np.append(x2,np.zeros((nframe*wlen/2)-nsampl))

   #Pre-processing for edges
   x1 = np.append(np.append(np.zeros(wlen/4), x1), np.zeros(wlen/4))
   x2 = np.append(np.append(np.zeros(wlen/4), x2), np.zeros(wlen/4))
   swin=np.zeros((nframe+1)*wlen/2)

   for t in np.arange(0,nframe):
       swin[(t*wlen/2):(t*wlen/2+wlen)] = swin[(t*wlen/2):(t*wlen/2+wlen)]+win**2

   swin=np.sqrt(wlen*swin)
   nbin=wlen/2+1

   X1 = np.zeros([nbin,nframe])
   X2 = np.zeros([nbin,nframe])
   X1 = X1 + 0j
   X2 = X2 + 0j


   for t in np.arange(0,nframe):
           #Framing
           frame1 = (x1[(t*wlen/2):(t*wlen/2+wlen)] *win) /swin[(t*wlen/2):(t*wlen/2+wlen)]
           frame2 = (x2[(t*wlen/2):(t*wlen/2+wlen)] *win) /swin[(t*wlen/2):(t*wlen/2+wlen)]
           #FFT
           fframe1=np.fft.fft(frame1)
           fframe2=np.fft.fft(frame2)
           X1[:,t]=fframe1[0:nbin]
           X2[:,t]=fframe2[0:nbin]

   return X1 , X2
 
def findpeaks(y,minD):
    xx = np.arange(0,len(y))
    iPk,iInfite,iInflect = getAllPeaks(y)
    #iPk = removePeaksBelowMinPeakHeight(y,iFinite,minH,refW)
    #iPk = removePeaksBelowThreshold(y,iPk,minT)
    idx = findPeaksSeparatedByMoreThanMinPeakDistance(y,xx,iPk,minD)
    iPk = iPk[idx]
    yy = y[iPk]
    inds = iPk[np.argsort(yy)[::-1]]
    peaks = y[inds]
    return peaks, inds
    

def  getAllPeaks(y):    
    #fetch indices all infinite peaks
    iInf = np.where(np.isinf(y).astype(int) & (y>0).astype(int))[0]

    # temporarily remove all +Inf values
    yTemp = y
    yTemp[iInf] = np.nan

    #determine the peaks and inflection points of the signal
    iPk,iInflect = findLocalMaxima(yTemp);
    
    return iPk,iInf,iInflect
    
def  findLocalMaxima(yTemp):
        
    yTemp = np.append(np.nan,np.append(yTemp,np.nan))    
    
    iTemp = np.arange(0,len(yTemp))

    # keep only the first of any adjacent pairs of equal values (including NaN).
    yFinite = (~np.isnan(yTemp)).astype(int)
    
    iNeq = np.append(0,1 + np.where((yTemp[0:len(yTemp)-1] != yTemp[1:len(yTemp)]) & (yFinite[0:len(yFinite)-1] | yFinite[1:len(yFinite)]))[0])
        
    iTemp = iTemp[iNeq]

    # take the sign of the first sample derivative
    s = np.sign(np.diff(yTemp[iTemp]))    

    # find local maxima
    iMax = 1 + np.where(np.diff(s)<0)[0]

    #find all transitions from rising to falling or to NaN
    iAny = 1 + np.where(s[0:len(s)-1] != s[1:len(s)])[0]

    # index into the original index vector without the NaN bookend.
    iInflect = iTemp[iAny]-1
    iPk = iTemp[iMax]-1
    
    return iPk, iInflect
  
    
def findPeaksSeparatedByMoreThanMinPeakDistance(y,x,iPk,Pd):
    
    if (iPk.size==0 or (Pd ==0)):
        idx = np.arange(0,len(iPk))
    
    pks = y[iPk]
    locs = x[iPk]
    
    sortIdx = np.argsort(pks)[::-1]
    locs_temp = locs[sortIdx]
    
    idelete = np.zeros(len(locs_temp)).astype(int)
    
    for i in  range(0,len(locs_temp)):
        if not idelete[i]:
            idelete = idelete | ((locs_temp>=(locs_temp[i]-Pd)).astype(int) & (locs_temp<=(locs_temp[i]+Pd)).astype(int))
            idelete[i] = 0
    
    idx = np.sort(sortIdx[np.where(idelete == 0)[0]])
    return idx
#---------------------------------------------- 

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Usage: {0} directory".format(sys.argv[0])
        quit()

dir_prefix = sys.argv[1]
inputFile = ["90.wav","150.wav", "120.wav"]

inputFile = ['0', '30', '60', '90', '120', '140',
    '150', '180', 'bathroom', 'd60',  'nextroom1', 'close_above_45',
    'd90', 'nextroom2', 'above135', 'd135', 'doorway', 'rightabove']


print("Angle is measured in degrees wrt to the perpendicular to the line joining the two microphones")
for i in range(0,len(inputFile)):
    
    (fs, x1, x2) = readwave(dir_prefix + '/' + inputFile[i] + '.wav')
    x1 = x1 + 0.0
    x2 = x2 + 0.0
    x1 = x1/32768
    x2 = x2/32768
    tau, angle = TDOA(x1, x2, fs, d, tau_grid)
    print "For", inputFile[i], "time delay of arrival is" , tau, "and angle is", angle
