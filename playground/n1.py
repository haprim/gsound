import wave
import numpy as np
import sys
import scipy.io.wavfile as wf
import matplotlib.pyplot as plt
import cmath
import math

def readwave(fn):
    rate, stream = wf.read(fn)
    data0 = stream[:, 0]
    data1 = stream[:, 1]
    assert(len(data0) == len(data1))

    return data0, data1

def stereo_fft(c1, c2, n):
    mx = len(c1)
    ctr = 30000
    tally = {}
    while True:
        f1 = np.fft.fft(c1[ctr:min(ctr+n, mx)])
        f2 = np.fft.fft(c2[ctr:min(ctr+n, mx)])
#        sm = sum(np.absolute(f1))
#        plt.plot(f1)
#        plt.plot(f2)
        plottie = []
        for x in range(50, 500):
            phd = cmath.phase(f1[x]) - cmath.phase(f2[x])
            if phd >= math.pi:
                phd -= 2*math.pi
            elif phd < -math.pi:
                phd += 2*math.pi
            plottie.append(phd)                
            if x not in tally:
                tally[x] = []
            tally[x].append(phd)                
        plt.plot(plottie)
#        for e in range(20, 500):
#            if e not in tally:
#                tally[e] = []
#            tally[e].append(cmath.phase(f1[x]                
#        plt.show()
#        print cmath.phase(f1[ind])
#        print cmath.phase(f2[ind])
#        plt.show()
        ctr += n
        if ctr >= mx:
            break

    ml = 0
    mv = 100
    ttt = []
    for k, v in tally.items():
        ss = np.std(v)
        mm = np.mean(v)
        if ss < mv:
            mv = ss
            ml = k
        print k, mm, ss
        ttt.append((mm, ss))

    print "min:", ml, mv        
    print tally.values()
    avgpd = np.average([x[0] for x in ttt])
    print "\nAVGM", avgpd
    print "S2", np.average([x[1] for x in ttt])

if __name__ == "__main__":
    fn = sys.argv[1]
    
    c1, c2 = readwave(fn)
    stereo_fft(c1, c2, 8192)
