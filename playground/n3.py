import wave
import numpy as np
import sys
import scipy.io.wavfile as wf
import scipy.signal as scs
import matplotlib.pyplot as plt
import cmath
import math

def readwave(fn):
    rate, stream = wf.read(fn)
    data0 = stream[:, 0]
    data1 = stream[:, 1]
    assert(len(data0) == len(data1))

    return data0, data1

def filter_peaks(arr, pks, minsep, thr):
    result = set([])
    for e in pks:
        if arr[e] >= thr:
            result.add(e)

    #now skip duplicates
    res2 = set([])
    std = sorted(result)
    i = 0
    j = 1
    keeps = set([])
    while True:
        if std[j] - std[i] < minsep:
            if arr[std[j]] > arr[std[i]]:
                if std[i] in keeps:
                    keeps.remove(std[i])
                keeps.add(std[j])          
            else:
                if std[j] in keeps:
                    keeps.remove(std[j])
                keeps.add(std[i])
        else:
            keeps.add(std[j])

        i = j
        j = i + 1
        if j >= len(std):
            break
                
    return [x for x in sorted(result)]

#this works for rectangular window only
def refine_peaks(arr, pks):
    result = []
    for e in pks:
        mx = e - 1
        sgn = -1
        if arr[e + 1] > arr[e - 1]:
            mx = e + 1
            sgn = 1
        lc = arr[e]
#        print "\t", e, mx, arr[mx],  sgn, lc
        rat = lc / float(arr[mx])

        offset = rat / (1.0 + rat)
        result.append(e + sgn * offset)
    return result        

def pick_peaks(arr, minbin, maxbin, minsep, wfn, rel_thr):
    ar2 = np.absolute(arr)
    peaks = set([])
    ml = -1
    mv = 0

    for i in range(int(minbin), len(ar2) - 1):
        if (ar2[i] > ar2[i-1]) and (ar2[i] > ar2[i+1]):
            peaks.add(i)
            if ar2[i] > mv:
                mv = ar2[i]
                ml = i
        if i > maxbin:
            break

    peaks = filter_peaks(ar2, peaks, minsep, mv * rel_thr)
#    if wfn == 0:
#        peaks = refine_peaks(ar2, peaks)
    return peaks            

def estimate_angle(vec, dst):
    nbins = 150
    mn = min(vec)
    mx = max(vec)
    bs = (mx - mn) / float(nbins)
    tally = [0 for _ in range(nbins)]

    for e in vec:
        bi = min(int((e - mn) / bs), nbins - 1)
        tally[bi] += 1
    mxc = np.argmax(tally)
    mdv = ((mxc + 0.5)*bs + mn)
#    print "MXC=", mxc, mdv, bs
    lth = (0.9 * mxc + 0.5)*bs + mn
    hth = (1.1 * mxc + 0.5)*bs + mn
    tot = [e for e in vec if (e >= lth and e <= hth)]
    avg = np.mean(tot)
    if avg > dst:
        avg = dst
    elif avg < -dst:
        avg = -dst

    ang = math.acos(avg / dst)
#    print "\t", avg
    return ang, avg
#FIXME: consider non-linear error, i.e. range not c*mxc, rather, corresponding
    #to phi +- delta


def stereo_fft(c1, c2, n, slide, result = {}):
    mx = len(c1)
    ctr = 000
    tally = {}

    wfn = 1
    if wfn == 1:
        window = scs.flattop(n)
        p_thr = 0.1
    else:
        window = np.array([1 for _ in range(n)])
        p_thr = 0.2

    binsize = 44100 / float(n)
    desbins = range(50, 1600, 50)

#    desbins = [200, 400, 825, 1100, 1650, 2500, 3300, 4950, 6600]
    for i in range(len(desbins)):
        desbins[i] = int(desbins[i] / binsize)

    maxamp = 0

    """
    while True:
        E = sum(np.absolute(c1[ctr:min(ctr + n, mx)]))
        E += sum(np.absolute(c2[ctr:min(ctr + n, mx)]))
        if E > maxamp:
            maxamp = E

        ctr += slide
        if ctr >= mx - n:
            break
    """            


    ctr = 0000
    plottie = []
    dst = 0.05
    while True:
        if ctr >= mx - n:
            break

        f1 = np.fft.fft(c1[ctr:min(ctr+n, mx)] * window)
        pks1 = pick_peaks(f1, 100 / float(binsize), 3300 / float(binsize), 
            5, wfn, p_thr)

        f2 = np.fft.fft(c2[ctr:min(ctr+n, mx)] * window)
        pks2 = pick_peaks(f1, 100 / float(binsize), 3300 / float(binsize), 
            5, wfn, p_thr)

#        print pks1
        bothpeaks = sorted(set(pks1).intersection(set(pks2)))
#        print pks2
#        print "both=", bothpeaks

        E = sum(np.absolute(c1[ctr:min(ctr + n, mx)]))
#        print E / n
        if E / n < 80: ##CUTOFF
            ctr += n
            continue

        for x in bothpeaks:
#            if x < 200:
#                continue
            phd = cmath.phase(f1[x]) - cmath.phase(f2[x])
            freq = x * binsize
            wl = 340.29 / freq

            if phd >= math.pi:
                phd -= 2*math.pi
            elif phd < -math.pi:
                phd += 2*math.pi

            wd = wl * phd / (2.0 * math.pi)
#            if (wd > 2 * dst) or (wd < -2*dst):
#                continue
            while wd > 2*dst:
                wd -= dst
            while wd < -2*dst:
                wd += dst

#            print x, phd, freq, wl, wd, wd / x
            plottie.append(wd)                
            if x not in tally:
                tally[x] = []
#            tally[x].append(100*phd / x)
            tally[x].append(100*wd)
#            tally[x].append(phd)
#        plt.plot(plottie)
#        for e in range(20, 500):
#            if e not in tally:
#                tally[e] = []
#            tally[e].append(cmath.phase(f1[x]                
#        plt.show()
#        print cmath.phase(f1[ind])
#        print cmath.phase(f2[ind])
#        plt.show()
        ctr += slide
#        print "\n\n"
#        if ctr > 35000:
#            quit()

    ang, avg = estimate_angle(plottie, dst)
    print "est angle:", ang, ang * 180/math.pi
#    print len(plottie)
    nn, bins, patches = plt.hist(plottie, 150)
    plt.show()

    """
    ml = 0
    mv = 100
    ttt = []
    for k in sorted(tally.keys()):
        if k > 3000:
            continue
        v = tally[k]
        ss = np.std(v)
        mm = np.mean(v)
        if ss < mv:
            mv = ss
            ml = k
        print k, mm, ss
        ttt.append((mm, ss, k))
        print "\t{0}: {1}, {2}".format(k, mm, ss)

    print "min:", ml, mv        
#    print tally.values()
    avgpd = np.average([x[0] for x in ttt])
    print "\nAVGM", avgpd

    print "S2", np.average([x[1] for x in ttt])

    print "PLOTST", np.mean(plottie), np.std(plottie)
    for e in ttt:
        if e[2] not in result:
            result[e[2]] = []
        result[e[2]].append(e[0])
    """    
    return result        


if __name__ == "__main__":
#    fn = sys.argv[1]
    path = sys.argv[1]

    filenames = ['0deg', 'm30deg', 'm60deg', 'm90deg', 'm120deg', 'm150deg',
        '180deg', 'neardoor1', 'nextroom1', 'nextroom2', 'close1_m120',
        'close2_m75', 'close3_m30']

#    filenames = ['0deg', 'm30deg', 'm60deg', 'm90deg', 'm120deg', 'm150deg',
#        '180deg']
        
    filenames = ['nextroom1', 'nextroom2']

    filenames = ['0', '30', '60', '90', '120', '140',
        '150', '180',
        'bathroom', 'd60',  'nextroom1', 'close_above_45',
        'd90', 'nextroom2', 'above135',
        'd135', 'doorway', 'rightabove']

#    filenames = ['nextroom1']        

    result = {}

    for e in filenames:
        fn = path + '/' + e + '.wav'
        c1, c2 = readwave(fn)
        print "FILE:", e
#        stereo_fft(c1, c2, 4096, 512, result)
        stereo_fft(c1, c2, 8192, 256, result)
#        stereo_fft(c1, c2, 32768, 256, result)

    print "\n\n==========\n\n"
    for k in sorted(result.keys()):
        v = result[k]
        print k, v

    for i in range(len(filenames)):
        print "\n\nFN:", filenames[i]
        v = [abs(result[k][i]) for k in result.keys()]
        print i, sum(v)

        x = result.keys()
        y = [result[k][i] for k in x]
        print len(x), len(y), x, y
        plt.scatter(x, y)
#        plt.show()
        print filenames[i], np.mean(y), np.std(y)
#        print np.mean(v), np.std(v)
