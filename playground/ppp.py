import wave
import numpy as np
import sys
import scipy.io.wavfile as wf
import scipy.signal as scs
import scipy.stats
import matplotlib.pyplot as plt
import cmath
import math

def readwave(fn):
    rate, stream = wf.read(fn)
    data0 = stream[:, 0]
    data1 = stream[:, 1]
    assert(len(data0) == len(data1))

    return data0, data1

def filter_peaks(arr, pks, minsep, thr):
    result = set([])
    for e in pks:
        if arr[e] >= thr:
            result.add(e)

    if len(result) < 2:
        return result

    #now skip duplicates
    res2 = set([])
    std = sorted(result)
    i = 0
    j = 1
    keeps = set([])
    while True:
        if std[j] - std[i] < minsep:
            if arr[std[j]] > arr[std[i]]:
                if std[i] in keeps:
                    keeps.remove(std[i])
                keeps.add(std[j])          
            else:
                if std[j] in keeps:
                    keeps.remove(std[j])
                keeps.add(std[i])
        else:
            keeps.add(std[j])

        i = j
        j = i + 1
        if j >= len(std):
            break
                
#    return [x for x in sorted(result)]
    return [x for x in sorted(keeps)]

#this works for rectangular window only
def refine_peaks(arr, pks):
    result = []
    for e in pks:
        mx = e - 1
        sgn = -1
        if arr[e + 1] > arr[e - 1]:
            mx = e + 1
            sgn = 1
        lc = arr[e]
#        print "\t", e, mx, arr[mx],  sgn, lc
        rat = lc / float(arr[mx])

        offset = rat / (1.0 + rat)
        result.append(e + sgn * offset)
    return result        

def pick_peaks(arr, minbin, maxbin, minsep, wfn, rel_thr):
    ar2 = np.absolute(arr)
    peaks = set([])
    ml = -1
    mv = 0
    mnl = -1
    mnv = 999999999999

    for i in range(int(minbin), len(ar2) - 1):
        if (ar2[i] > ar2[i-1]) and (ar2[i] > ar2[i+1]):
            peaks.add(i)
            if ar2[i] > mv:
                mv = ar2[i]
                ml = i
            if ar2[i] < mnv:
                mnv = ar2[i]
                mnl = i

        if i > maxbin:
            break

    peaks = filter_peaks(ar2, peaks, minsep, mv * rel_thr)
#    if wfn == 0:
#        peaks = refine_peaks(ar2, peaks)
    if mv < 0.000001:
        rat = 1
    else:
        rat = mnv / float(mv)
    return peaks, rat

def estimate_angle_old(vec, wts, dst):
    nbins = 50
    mn = min(vec)
    mx = max(vec)
    bs = (mx - mn) / float(nbins)
    tally = [[] for _ in range(nbins)]

    for i, e in enumerate(vec):
        bi = min(int((e - mn) / bs), nbins - 1)
        tally[bi].append((e, wts[i]))

#    t2 = [len(x) for x in tally]
    t2 = [sum([y[1] for y in x]) for x in tally]
    print t2
    mxc = np.argmax(t2)
    mxv = t2[mxc]

    mnc = int(round(0.2 * mxc))
    mdv = ((mxc + 0.5)*bs + mn)
    lth = (0.8 * mxc + 0.5)*bs + mn
#    lth = (mxc - 10 + 0.5)*bs + mn
    hth = (1.2 * mxc + 0.5)*bs + mn
#    hth = (mxc + 10 + 0.5)*bs + mn
#    print "MXC=", mxc, mdv, bs, lth, hth
    w2 = []
    tot = []
    for iii in range(len(vec)):
        e = vec[iii]
        if (e >= lth) and (e <= hth):
            tot.append(e)
            w2.append(wts[iii])
#    tot = [e for e in vec if (e >= lth and e <= hth)]

#    tot = [e for e in vec]
    """
    tot = []
    for eee in tally:
        if len(eee) >= mnc:
            tot.extend(eee)
    """
    mu, std = scipy.stats.norm.fit(tot)
    print "MSTD", mu, std

#    """ ***
    nn, bins, patches = plt.hist(vec, bins=nbins)
    xmn, xmx = plt.xlim()
    xx=np.linspace(xmn,xmx,100)
    pp=scipy.stats.norm.pdf(xx,mu,std)
    plt.plot(xx,pp,'k',linewidth=2)
    plt.show()
#    """ 


#    avg = np.mean(tot)
    avg = np.average(tot, weights = w2)
#    print w2
#    avg = np.average(tot)
    std = np.std(tot)
    if avg > dst:
        avg = dst
    elif avg < -dst:
        avg = -dst

    ang = math.acos(avg / dst)
#    print "\t", avg, lth, hth
    return ang, avg, std
#FIXME: consider non-linear error, i.e. range not c*mxc, rather, corresponding
    #to phi +- delta

def functionize_hits(tally):
    result = [0.0 for _ in tally]

    #1 pack into 1D
    d1rep = []
    for i in range(len(result)):
        if len(tally[i]) < 1:
            d1rep.append((-10, 0))
            continue
        tavg = np.average([x[0] for x in tally[i]])
        swt = sum([x[1] for x in tally[i]])
        d1rep.append((tavg, swt))

    #2 smoothen
    smsz = 5
    assert(smsz % 2 == 1)
    offs = smsz // 2
    d1r2 = []
    ldr = len(d1rep)

    for i in range(ldr):
        if i < offs:
            beg = 0
            end = i + 1
        elif i > ldr - 1 - offs:
            end = ldr
            beg = i
        else:
            beg = i - offs
            end = i + offs + 1

        tot = 0.0
        totwt = 0.0
        for j in range(beg, end):
            tot += d1rep[j][0] * d1rep[j][1]
            totwt += d1rep[j][1]

        if totwt < 0.001:
            d1r2.append((tot, 0))
        else:
            tot /= totwt
            d1r2.append((tot, totwt / (end - beg)))
#        print i, j, d1rep[i][0], tot, totwt, beg, end

    #3. filter1: peaks and filtering
    ml = np.argmax([x[1] for x in d1r2])
    mE = d1r2[ml][1]
    thr = 0.1 * mE #thr for fn
    thr2 = 0.4 * mE #thr for peaks

    peaks = []
    for i in range(len(d1r2)):
        if (i == 0) or (i == ldr - 1):
            continue
        elif d1r2[i][1] < thr2:
            continue
        else:
            wt = -1
            #local maximum? surely a peak
#            if (d1r2[i][1] > d1r2[i-1][1]) and (d1r2[i][1] > d1r2[i+1][1]):
            if (0):
                wt = 1
            else:                        
                dy = d1r2[i+1][1] - d1r2[i-1][1]
                dx = d1r2[i+1][0] - d1r2[i-1][0]

                dst = abs(d1r2[i][0]*dy - d1r2[i][1]*dx + 
                    d1r2[i-1][1]*d1r2[i+1][0] - d1r2[i+1][1]*d1r2[i-1][0]) /\
                    ((dx**2 + dy**2)**0.5)

                elevation = 2 * d1r2[i][1] - (d1r2[i-1][1] + d1r2[i+1][1])
#                print i, d1r2[i][0], dst, elevation
                if (elevation > 0) and (d1r2[i][1] >= thr): #peak and strong
                    wt = elevation / mE

            if wt > -1:                    
                peaks.append((d1r2[i][0], d1r2[i][1], wt))

#    print "d1r2=", d1r2
    d1r3 = [(d1r2[i][0], d1r2[i][1]) if d1r2[i][1] >= thr else (d1r2[i][0], 0) 
        for i in range(len(d1r2))]

    return d1r3, peaks

def calc_angle(csval, dst):
    cc = csval
    if csval > dst:
        csval = dst
    elif csval < -dst:
        csval = -dst
    return 180.0 * math.acos(csval / dst) / math.pi

def do_estimations(fn, vec, bs):
    result = []

    state = 0
    maxwt = 0
    for e in fn:
        if e[1] > 0.001:
            if state == 0:
                rangemin = e[0] - 0.5*bs
                cumwt = 0
            rangemax = e[0] + 0.5*bs
            cumwt += e[1]
            state += 1
        else:
            if state > 0:
                ll = [x for x in vec if (x >= rangemin) and (x <= rangemax)]
                if len(ll) > 0:
                    mn = np.mean(ll)
                    st = np.std(ll)
                    wt = cumwt
                    if wt > maxwt:
                        maxwt = wt
                    result.append((mn, st, wt))
            state = 0
#        print e

    for i in range(len(result)):
        result[i] = (result[i][0], result[i][1], result[i][2] / maxwt)

    ret = []
    dst = 0.05
    for e in result:
        mphi = calc_angle(e[0], dst)
        mphim = calc_angle(e[0] - e[1], dst)
        mphip = calc_angle(e[0] + e[1], dst)
        ret.append((mphi, mphim, mphip, e[2]))
    return ret

def estimate_angle(vec, wts, dst):
    nbins = 150
    mn = min(vec)
    mx = max(vec)
    bs = (mx - mn) / float(nbins)
    tally = [[] for _ in range(nbins)]

    for i, e in enumerate(vec):
        bi = min(int((e - mn) / bs), nbins - 1)
        tally[bi].append((e, wts[i]))

    fned, pks = functionize_hits(tally)

    angles = do_estimations(fned, vec, bs)
#    print "angles", angles
    
    tot = [x[0] for x in fned]
    plt.plot(tot, [x[1] for x in fned])
    xmn, xmx = plt.xlim()
    xx=np.linspace(xmn,xmx,100)
#    plt.plot(xx,pp,'k',linewidth=2)
    plt.show()
#    """ 
    return angles

    return ang, avg, std
#FIXME: consider non-linear error, i.e. range not c*mxc, rather, corresponding
    #to phi +- delta




def stereo_fft(c1, c2, n, padded_size, slide, result = {}):
    diff = padded_size - n
    diffp2 = diff // 2
    mx = len(c1)
    ctr = 000
    tally = {}

    wfn = 1
    if wfn == 1:
        window = scs.flattop(padded_size)
        p_thr = 0.1
    elif wfn == 2:
        wa = [0 for _ in range(padded_size)]
        center = padded_size // 2
        wdth = int(0.025 * padded_size)
        for kk in range(wdth):
            wa[center+kk] = 1
            wa[center-kk] = 1
            wa[center+wdth+kk] = 1.0 - kk / float(wdth)
            wa[center-wdth-kk] = 1.0 - kk / float(wdth)
        window = np.array(wa)
        p_thr = 0.1

    else:
        window = np.array([1 for _ in range(padded_size)])
        p_thr = 0.2

    binsize = 96000 / float(padded_size)
    desbins = range(50, 1600, 50)

#    desbins = [200, 400, 825, 1100, 1650, 2500, 3300, 4950, 6600]
    for i in range(len(desbins)):
        desbins[i] = int(desbins[i] / binsize)

    maxamp = 0

    """
    while True:
        E = sum(np.absolute(c1[ctr:min(ctr + n, mx)]))
        E += sum(np.absolute(c2[ctr:min(ctr + n, mx)]))
        if E > maxamp:
            maxamp = E

        ctr += slide
        if ctr >= mx - n:
            break
    """            


    ctr = 0000
    plottie = []
    wts = []
    dst = 0.05
    minfreq = 500
    maxfreq = 3000
    while True:
        if ctr >= mx - n:
            break
        dd1 = np.lib.pad(c1[ctr:min(ctr+n, mx)], 
            (diffp2, diff - diffp2), 'constant',  constant_values=(0,0))
        dd2 = np.lib.pad(c2[ctr:min(ctr+n, mx)], 
            (diffp2, diff - diffp2), 'constant', constant_values=(0,0))

        f1 = np.fft.fft(dd1 * window)

        pks1, r1 = pick_peaks(f1, minfreq / float(binsize), 
            maxfreq / float(binsize), 
            5, wfn, p_thr)

        f2 = np.fft.fft(dd2 * window)
        pks2, r2 = pick_peaks(f1, minfreq / float(binsize), 
            maxfreq / float(binsize), 
            5, wfn, p_thr)

#        print r1, r2
#        if (r1 > 0.005) or (r2 > 0.005):
#            ctr += slide
#            continue

        bothpeaks = sorted(set(pks1).intersection(set(pks2)))
#        print pks2
#        print "both=", bothpeaks

        E = sum(np.absolute(c1[ctr:min(ctr + n, mx)]))
#        print E / n
        if E / n < 80: ##CUTOFF
            ctr += slide
            continue

        for x in bothpeaks:
#            if x < 200:
#                continue
            phd = cmath.phase(f1[x]) - cmath.phase(f2[x])
#            print cmath.phase(f1[x]), cmath.phase(f2[x]), phd
            freq = x * binsize
            wl = 340.29 / freq

            if phd >= math.pi:
                phd -= 2*math.pi
            elif phd < -math.pi:
                phd += 2*math.pi

            wd = wl * phd / (2.0 * math.pi)
#            print x, freq, cmath.phase(f1[x]), cmath.phase(f2[x]), wl, wd
#            if (wd > 2 * dst) or (wd < -2*dst):
#                continue
            while wd > 2*dst:
                wd -= dst
            while wd < -2*dst:
                wd += dst

#            print x, phd, freq, wl, wd, wd / x
            plottie.append(wd)                
            wts.append(np.abs(f1[x] + np.abs(f2[x]))/2.0)
            if x not in tally:
                tally[x] = []
#            tally[x].append(100*phd / x)
            tally[x].append(100*wd)
#            tally[x].append(phd)
#        plt.plot(plottie)
#        for e in range(20, 500):
#            if e not in tally:
#                tally[e] = []
#            tally[e].append(cmath.phase(f1[x]                
#        plt.show()
#        print cmath.phase(f1[ind])
#        print cmath.phase(f2[ind])
#        plt.show()
        ctr += slide
#        print "\n\n"
#        if ctr > 35000:
#            quit()

    angles = estimate_angle(plottie, wts, dst)
    st = list(reversed(sorted([(eee[3], eee) for eee in angles])))
    for eee in st:
        print "Prob {0}: Angle at {1} (68.26% likely within {2}-{3}".format(
            eee[0], eee[1][0], eee[1][1], eee[1][2])
#    print len(plottie)
#    nn, bins, patches = plt.hist(plottie, 150)
#    plt.show()

    """
    ml = 0
    mv = 100
    ttt = []
    for k in sorted(tally.keys()):
        if k > 3000:
            continue
        v = tally[k]
        ss = np.std(v)
        mm = np.mean(v)
        if ss < mv:
            mv = ss
            ml = k
        print k, mm, ss
        ttt.append((mm, ss, k))
        print "\t{0}: {1}, {2}".format(k, mm, ss)

    print "min:", ml, mv        
#    print tally.values()
    avgpd = np.average([x[0] for x in ttt])
    print "\nAVGM", avgpd

    print "S2", np.average([x[1] for x in ttt])

    print "PLOTST", np.mean(plottie), np.std(plottie)
    for e in ttt:
        if e[2] not in result:
            result[e[2]] = []
        result[e[2]].append(e[0])
    """    
    return result        


if __name__ == "__main__":
#    fn = sys.argv[1]
    path = sys.argv[1]

    filenames = ['0deg', 'm30deg', 'm60deg', 'm90deg', 'm120deg', 'm150deg',
        '180deg', 'neardoor1', 'nextroom1', 'nextroom2', 'close1_m120',
        'close2_m75', 'close3_m30']

#    filenames = ['0deg', 'm30deg', 'm60deg', 'm90deg', 'm120deg', 'm150deg',
#        '180deg']
        
    filenames = ['nextroom1', 'nextroom2']

    filenames = ['0', '30', '60', '90', '120', '140',
        '150', '180',
        'bathroom', 'd60',  'nextroom1', 'close_above_45',
        'd90', 'nextroom2', 'above135',
        'd135', 'doorway', 'rightabove']

#    filenames = ['nextroom1']        
    filenames = ['aaa']

    result = {}

#    path='../data/'
#    filenames=['puretone']

    for e in filenames:
        fn = path + '/' + e + '.wav'
        c1, c2 = readwave(fn)
        print "FILE:", e
#        stereo_fft(c1, c2, 4096, 512, result)
#        stereo_fft(c1, c2, 1024, 8192, 256, result)
#        stereo_fft(c1, c2, 4096, 32768, 2048, result)
#        stereo_fft(c1, c2, 1323, 32768, 1000, result)
#        stereo_fft(c1, c2, 4096, 16000, 2048, result)
        stereo_fft(c1, c2, 32768, 65536, 2560, result)

    print "\n\n==========\n\n"
    for k in sorted(result.keys()):
        v = result[k]
        print k, v

    for i in range(len(filenames)):
        print "\n\nFN:", filenames[i]
        v = [abs(result[k][i]) for k in result.keys()]
        print i, sum(v)

        x = result.keys()
        y = [result[k][i] for k in x]
        print len(x), len(y), x, y
        plt.scatter(x, y)
#        plt.show()
        print filenames[i], np.mean(y), np.std(y)
#        print np.mean(v), np.std(v)
