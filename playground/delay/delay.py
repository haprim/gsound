import wave
import numpy as np
import sys
import scipy.io.wavfile as wf
import scipy.signal as scs
import matplotlib.pyplot as plt
import cmath
import math

def readwave(fn):
    rate, stream = wf.read(fn)
    data0 = stream[:, 0]
    data1 = stream[:, 1]
    assert(len(data0) == len(data1))

    return data0, data1

def delays(c1, c2, n, maxd):
    mx = len(c1)
    ctr = n + maxd
    tally = {}
    rn = range(n)
    prevmaxloc = 0
    delays = []
    tots = []
    totwt = 0
    mh = {}

    while True:
        if ctr >= mx - maxd:
            break

        a1 = c1[ctr - n:ctr]
#        flt = sum(a1) / len(a1)
#        if flt < 300:
#            ctr += n
#            continue

        a2 = c2[ctr - n - maxd:ctr + maxd + 1] / 100000.0

        maxprod = 0
        maxloc = -1
        for e in range(-maxd, maxd + 1, 1):
            prod = sum([a1[i] * a2[i + e] for i in rn])
            print "\t{0} - {1}: {2}".format(ctr, e, prod)
            if prod > maxprod:
                maxprod = prod
                maxloc = e

        if maxloc not in mh:
            mh[maxloc] = 0

        totwt += maxprod
        tots.append(maxprod * maxloc)
        mh[maxloc] += maxprod
        delays.append(maxloc)
        if maxloc != prevmaxloc:
            print "At {0}: shift={1}".format(ctr, maxloc)

        ctr += 5 * n

    print "delays:", np.mean(delays), np.std(delays)        
    print "tots:", np.mean(tots) / totwt, np.std(tots)
#    n, bins, patches = plt.hist(delays)
    for i in range(len(tots)):
        tots[i] /= totwt

#    n, bins, piatches = plt.hist(tots, 5)
#    plt.plot(bins)
#    plt.show()
    st = sorted(mh.keys())
    x = np.arange(len(st))
    y = np.array([mh[k] for k in st])
    plt.scatter(x, y)
    plt.show()


def crosscorr(c1, c2, n):
    mx = len(c1)
    ctr = 000
    tally = {}
    exts = []
    tots = []
    totwt = 0

    while True:
#        print ctr
        if ctr >= mx - n:
            break

        a1 = list(c1[ctr:ctr + n])

        a2 = list(c2[ctr:ctr + n])
        E1 = sum(a1) / 100000.0
        E2 = sum(a2) / 100000.0

        a1.extend(np.zeros(1*n))
        a2.extend(np.zeros(1*n))
        a1 = np.array(a1)
        a2 = np.array(a2)

#        print a1
#        print a2
        f1 = np.fft.fft(a1, len(a1))
        f2 = np.fft.fft(a2, len(a1))

#        cc = []
#        for i in range(n):
#            cc.append(f1[i] * f2[i].conjugate())
        f3 = np.fft.ifft(f1 * np.conjugate(f2))
#        f3 = np.fft.irfft(f1 * f2)

#        exts.append(f3.argmin())
#        print "MIN=", f3.argmin(), ctr

        aaa = scs.correlate(f1, f2.conjugate(), mode='same')
        xxx= aaa.argmax()
        print "MMM", xxx
        if (xxx < n - 10) or (xxx > n + 10):
            ctr += 1 * n
            continue

        exts.append(xxx)
        tots.append(xxx * E1 * E2)
        totwt += E1 * E2
#        print "MIN=", np.argmin(f3), len(f3)
#        xx = np.arange(len(f3))
#        yy = np.absolute(f3)
#        plt.plot(xx, yy)
#        plt.show()

        ctr += 1 * n

    print "AVG", np.mean(exts), np.std(exts)
    tots /= totwt
    print "TOTS:", sum(tots)

if __name__ == "__main__":
    fn = sys.argv[1]
    
    c1, c2 = readwave(fn)

#    delays(c1, c2, 512, 7)
    crosscorr(c1, c2, 256)
