import wave
import numpy as np
import sys
import scipy.io.wavfile as wf
import scipy.signal as scs
import scipy.stats
import matplotlib.pyplot as plt
import cmath
import math

vsound = 340.29

def max_offset(SR, vsound, micdist):
    return micdist * SR / float(vsound)


def readwave(fn):
    rate, stream = wf.read(fn)
    data0 = stream[:, 0]
    data1 = stream[:, 1]
    assert(len(data0) == len(data1))

    return data0, data1, rate

def filter_peaks(arr, pks, minsep, thr):
    result = set([])
    for e in pks:
        if arr[e] >= thr:
            result.add(e)

    if len(result) < 2:
        return result

    #now skip duplicates
    res2 = set([])
    std = sorted(result)
    i = 0
    j = 1
    keeps = set([])
    while True:
        if std[j] - std[i] < minsep:
            if arr[std[j]] > arr[std[i]]:
                if std[i] in keeps:
                    keeps.remove(std[i])
                keeps.add(std[j])          
            else:
                if std[j] in keeps:
                    keeps.remove(std[j])
                keeps.add(std[i])
        else:
            keeps.add(std[j])

        i = j
        j = i + 1
        if j >= len(std):
            break
                
#    return [x for x in sorted(result)]
    return [x for x in sorted(keeps)]


def pick_peaks(arr, minbin, maxbin, minsep, wfn, rel_thr):
    ar2 = np.absolute(arr)
    peaks = set([])
    ml = -1
    mv = 0
    mnl = -1
    mnv = 999999999999

    for i in range(int(minbin), len(ar2) - 1):
        if (ar2[i] > ar2[i-1]) and (ar2[i] > ar2[i+1]):
            peaks.add(i)
            if ar2[i] > mv:
                mv = ar2[i]
                ml = i
            if ar2[i] < mnv:
                mnv = ar2[i]
                mnl = i

        if i > maxbin:
            break

    peaks = filter_peaks(ar2, peaks, minsep, mv * rel_thr)
    if mv < 0.000001:
        rat = 1
    else:
        rat = mnv / float(mv)
    return peaks, rat, mv


#tag is a directional number
def stereo_fft(c1, c2, ctr, n, padded_size, stn, tag, result):
    diff = padded_size - n
    diffp2 = diff // 2
    mx = len(c1)
    tally = {}

    wfn = 1
    if wfn == 1:
        window = scs.flattop(padded_size)
        p_thr = 0.3
    elif wfn == 2:
        wa = [0 for _ in range(padded_size)]
        center = padded_size // 2
        wdth = int(0.025 * padded_size)
        for kk in range(wdth):
            wa[center+kk] = 1
            wa[center-kk] = 1
            wa[center+wdth+kk] = 1.0 - kk / float(wdth)
            wa[center-wdth-kk] = 1.0 - kk / float(wdth)
        window = np.array(wa)
        p_thr = 0.1

    else:
        window = np.array([1 for _ in range(padded_size)])
        p_thr = 0.2

    binsize = 44100 / float(padded_size)

    maxamp = 0

    minfreq = 100
    maxfreq = 3000
#    print "ctr,n,mx", ctr,n,mx

    dd1 = np.lib.pad(c1[ctr:min(ctr+n, mx)], 
        (diffp2, diff - diffp2), 'constant',  constant_values=(0,0))
    dd2 = np.lib.pad(c2[ctr:min(ctr+n, mx)], 
        (diffp2, diff - diffp2), 'constant', constant_values=(0,0))

    f1 = np.fft.fft(dd1 * window)

    pks1, r1, maxv1 = pick_peaks(f1, minfreq / float(binsize), 
        maxfreq / float(binsize), 
        5, wfn, p_thr)

    f2 = np.fft.fft(dd2 * window)
    pks2, r2, maxv2 = pick_peaks(f2, minfreq / float(binsize), 
        maxfreq / float(binsize), 
        5, wfn, p_thr)

#        print maxv1, maxv2
#        if (r1 > 0.005) or (r2 > 0.005):
#            ctr += slide
#            continue

    bothpeaks = (set(pks1).intersection(set(pks2)))

    sorter = list(reversed(sorted([(np.abs(f1[x] + np.abs(f2[x]))/2.0, x) for x in bothpeaks])))
    if len(sorter) < stn:
        return

    meml = []
    for i in range(stn - 1):
        for j in range(i + 1, stn):
            mni = i
            mxi = j
            if sorter[i][0] > sorter[j][0]:
                mni = j
                mxi = i
            fr_ratio = int(10 * sorter[mxi][0] / float(sorter[mni][0]))
            amp_ratio = int(10 * sorter[mxi][1] / float(sorter[mni][1]))
            meml.append((fr_ratio, amp_ratio))
    st2 = sorted(meml)                
    key = tuple([x[0] for x in st2])
    val = tuple([x[1] for x in st2])
    if key not in result:
        result[key] = set([])
    result[key].add(tag)        

    return result        


def amp_impulses(c1, c2, amp_thr, diff_thr, winsize, min_dur_factor):
    assert(len(c1) == len(c2))

    min_duration = int(min_dur_factor * winsize)

    IDB_DEF = -200
    result = []
    ctr = 0
    lastdb = IDB_DEF
    lastdiff = 0
    impulse_start = -1
    last_time = -min_duration
    block_until = 0

    #states: 0 - off, 1 - in impulse
    state = 0 #off

    while True:
#        print ctr
#        E = ((sum((c1[ctr:ctr + winsize] / 256.0**2)**2) + \
#            0*sum((c2[ctr:ctr + winsize] / 256.0**2)**2)) / (4 * len(c1)**2))**0.5
        E1 = 2 * sum((c1[ctr:ctr + winsize] / 131072.0)**2)
        E2 = 2 * sum((c2[ctr:ctr + winsize] / 131072.0)**2)

        #multiply avg by 2/N, i.e. multiply sum by 1/N
        db = 10 * math.log((E1 + E2) / len(c1), 10)

        print ctr, db, lastdb
        if (db < amp_thr):
            if ctr < block_until:
                ctr += winsize
                continue

#            ...
            if state == 1:
                result.append((impulse_start, lastdb, lastdiff))

            state = 0
            impulse_start = -1
            lastdb = db
            lastdiff = 0
        else:
            if (db - lastdb > 2 * lastdiff):
                state = 1
                impulse_start = ctr
                lastdiff = db - lastdb
                lastdb = db
                block_until = impulse_start + min_duration

        """
        if (db >= amp_thr) and (db >= IDB_DEF): #
            
#            print ctr, db, lastdb, diff_thr
#            if db - lastdb > diff_thr:
            if db - lastdb > 0:
#                print "\tOK"
                if ctr - last_time >= min_duration:
                    last_time = ctr
                    impulse_start_db = db
                    result.append(ctr)

        lastdb = db
        """
        ctr += winsize
        if ctr + winsize >= len(c1):
            break
    return result 

def max_corr_offset(c1, c2, ind, lc, rng):
    assert(len(c1) == len(c2))
    irange = int(math.ceil(rng))

    maxcoeff = -1
    mxloc = -2 * rng
    for i in range(0, irange + 1):
        a1 = np.array(c1[ind: ind + lc - i])
        a2 = np.array(c2[ind + i: ind + lc])
#        print len(a1), len(a2), ind, ind + i, ind + lc, ind + lc - i
        coeff = np.corrcoef(a1, a2)[0, 1]
        if coeff > maxcoeff:
            maxcoeff = coeff
            mxloc = i

    for i in range(1, irange + 1):
        a1 = np.array(c1[ind + i: ind + lc])
        a2 = np.array(c2[ind: ind + lc - i])
#        print len(a1), len(a2), ind, ind + i, ind + lc, ind + lc - i
        coeff = np.corrcoef(a1, a2)[0, 1]
        if coeff > maxcoeff:
            maxcoeff = coeff
            mxloc = -i

    return maxcoeff, mxloc

def calc_angle(offs, mxoffs):
    rat = offs / mxoffs
    if rat > 1:
        rat = 1.0
    elif rat < -1:
        rat = -1.0

    return math.acos(rat)        



if __name__ == "__main__":
#    fn = sys.argv[1]
    path = sys.argv[1]

    filenames = ['0deg', 'm30deg', 'm60deg', 'm90deg', 'm120deg', 'm150deg',
        '180deg', 'neardoor1', 'nextroom1', 'nextroom2', 'close1_m120',
        'close2_m75', 'close3_m30']

#    filenames = ['0deg', 'm30deg', 'm60deg', 'm90deg', 'm120deg', 'm150deg',
#        '180deg']
        
    filenames = ['nextroom1', 'nextroom2']

    filenames = ['0', '30', '60', '90', '120', '140',
        '150', '180',
        'bathroom', 'd60',  'nextroom1', 'close_above_45',
        'd90', 'nextroom2', 'above135',
        'd135', 'doorway', 'rightabove']

    filenames = ['0f1', '0f2']
    filenames = ['hatter1', 'hatter2', 'hatter3', 'hatter4']

#    filenames = ['nextroom1']        

    result = {}

#    path='../data/'
#    filenames=['puretone']

    for e in filenames:
        fn = path + '/' + e + '.wav'
        c1, c2 = readwave(fn)
#        stereo_fft(c1, c2, 4096, 512, result)
#        stereo_fft(c1, c2, 1024, 8192, 256, result)
#        stereo_fft(c1, c2, 4096, 32768, 2048, result)
#        stereo_fft(c1, c2, 1323, 32768, 1000, result)
        ctr = 0
        n = 4096
        step = 4096
        pads = 32000
        while True:
            rr = stereo_fft(c1, c2, ctr, n, pads, 4, ctr, result)
            ctr += step
            if ctr + n >= len(c1):
                break
#        stereo_fft(c1, c2, 32768, 32768, 25600, result)

    print "known_backgrounds=", result

