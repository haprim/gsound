import sys
import numpy as np

#sect_dists = [20, 17, 14, 8, 0]
sect_dists = [18, 15, 12, 7, 0]
maxdist = 23

def sectorize(bmp):
    ld2 = 2 * len(sect_dists) - 1
    result = [[] for _ in range(ld2)]
    midpt = (bmp.shape[0] / 2 - 0.5, bmp.shape[1] / 2 - 0.5)
    radius = (bmp.shape[0] // 2, bmp.shape[1] // 2)
    sds = [x**2 for x in sect_dists]
    md = maxdist ** 2

    for i in range(bmp.shape[0]):
        for j in range(bmp.shape[1]):
            d2 = (midpt[0] - i)**2 + (midpt[1] - j)**2
            sector = -1
            for k in range(len(sds)):
                if d2 > sds[k]:
                    sector = k
                    break
            if j > midpt[1]:
                sector = ld2 - sector - 1
            assert(sector != -1)                    
            result[sector].append(bmp[i,j])
#            print i, j, bmp[i, j], d2 > md, sector
#    quit()            

    return result

def v_sectorize(bmp, n_sec = 8):
    thr = bmp.shape[1] / n_sec
    result = [[] for _ in range(n_sec)]
    for i in range(bmp.shape[0]):
        for j in range(bmp.shape[1]):
            sector = int(j / thr)
            result[sector].append(bmp[i,j])
#            print i, j, bmp[i, j], d2 > md, sector
#    quit()            
    return result            


def generate_sectors(xres, yres):
    result = {i: [] for i in range(2 * len(sect_dists) - 1)}
    sds = [x**2 for x in sect_dists]
    ld2 = 2 * len(sect_dists) - 1
    for i in range(xres):
        for j in range(yres):
            d2 = (midpt[0] - i)**2 + (midpt[1] - j)**2
            sector = -1
            for k in range(len(sds)):
                if d2 > sds[k]:
                    sector = k
                    break
            if j > midpt[1]:
                sector = ld2 - sector - 1
            assert(sector != -1)                    
            result[sector].append((i, j))
    return result

def generate_v_sectors(xres, yres, n_sec = 8, leave_edges = False):
    thr = yres / n_sec
    result = {i: [] for i in range(n_sec)}
    for i in range(xres):
        
        if leave_edges:
            if (i < 3) or (i >= xres - 3):
                continue
        for j in range(yres):
            sector = int(j / thr)
            result[sector].append((i, j))
    return result



def datdiff(d1, d2, thr):
    result = []
    for i in range(len(d1)):
#        print d1
        df = sum(np.abs(np.subtract(d1[i], d2[i])))
        result.append((df, len(d1[i])))
    return result        

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Usage: {0} filename".format(sys.argv[0])
        quit()

    fn = sys.argv[1]

    arr = np.load(fn)
    lastsec = -1

    for i in range(1, len(arr)):
        s0 = v_sectorize(arr[i - 1])
        s1 = v_sectorize(arr[i])
#    print [len(X) for X in sects0]
#    print sects0
        thr = 3
        ddff = datdiff(s1, s0, thr)

        active_sectors = []
        for j, e in enumerate(ddff):
            xx = e[0] / e[1]
            if xx > thr:
                active_sectors.append((j, xx))

        if len(active_sectors) > 0:
            mxsloc = np.argmax([x[1] for x in active_sectors])
            asc = active_sectors[mxsloc]
            if asc[0] == lastsec:
                continue
            print "{0},{1},{2}".format(5 * i, asc[0], asc[1])
#            print "{2}::: SIGNAL IN SECTOR {0}, {1}".format(
#                asc[0], asc[1], 5 * i)
            lastsec = asc[0] 
#            for e in active_sectors:
#                print "TIME={0}, SECTOR={1}".format(5*i, e[0]), e[1]
#            print "\t", e, e[0] / e[1]
