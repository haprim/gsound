import sys
import numpy as np
from vid_proc_large import *
import codecs
import csv
import cv2
import time

def load_sound_events(csvdata, nframes):
    result = []
    for e in csvdata:
        (start, ang, power, ilow, ihi) = tuple([float(x) for x in e])
        ns = int(nframes * start)
        result.append((ns, ang, power, int(nframes * ilow), 
            int(nframes * ihi)))
    return result

def paint_sector(img, sectors, sec, power):
    thr = 0.01
    step = 0.05
    if power < thr:
        return

    for e in sectors[sec]:
        lst = img[e[0], e[1]]
        res = []
        xmax = max(lst)
        rr = 255 - xmax
        div = rr / (5 * step)
        vv = xmax + div * (power - thr)
        res.append([0, 0, 255])
#        res.append([vv, vv, vv])
        img[e[0], e[1]] = np.array(res)


def paint_stripe(img, xcoord, power):
    thr = 0.005
    step = 0.025
    if power < thr:
        return

    for ii in range(-5, 6):
        if (xcoord + ii < 0) or (xcoord + ii > img.shape[1] - 1):
            continue
        for jj in range(img.shape[0]):
            lst = img[jj, xcoord + ii]
            res = []
            xmax = max(lst)
            rr = 255 - xmax
            div = rr / (5 * step)
            vv = xmax + div * (power - thr)
            res.append([vv, vv, vv])
#        res.append([vv, vv, vv])
            img[jj, xcoord + ii] = np.array(res)



def get_sector(ang):
    mir = 0
    if ang > 90:
        mir = 1

    if ang < 37.5:
        ret = 0
    elif ang < 48.5:
        ret = 1
    elif ang < 63.5:
        ret = 2
    elif ang < 75.5:
        ret = 3
    else:
        ret = 4

    if mir:
        ret = 8 - ret
    return ret        

def paint_sector2(img, angle, power):
    thr = 0.01
    step = 0.05
    if power < thr:
        return

    if angle < 30.0:
        newx = 1
    elif angle > 150.0:
        newx = 38
    else:
        angle -= 90.0
        rad = angle * np.pi / 180.0
        newx = 19.5 * (1.0 + (2 / (3**0.5)) * np.sin(rad))
        newx = max(1, newx)
        newx = min(38, newx)

    for i in range(30):
        lst = img[i, newx]
        xmax = max(lst)
        rr = 255 - xmax
        div = rr / (5 * step)
        vv = xmax + div * (power - thr)
        res = np.array([vv, vv, vv])
        for j in [-1, 0, 1]:
            img[i, newx + j] = res


if __name__ == "__main__":
    if len(sys.argv) < 4:
        print "Usage: {0} filename sound_events_csv video_events_csv".format(
            sys.argv[0])

        quit()

    sectors = generate_v_sectors(200, 320, leave_edges = True)
    vidfn = sys.argv[1]
    evfn = sys.argv[2]
    videvfn = sys.argv[3]

    lastsec = -1

    csvd = csv.reader(codecs.open(evfn, 'rt'))
    evs = load_sound_events(csvd, 25)

    csvd = csv.reader(codecs.open(videvfn, 'rt'))
    videvs = []
    for e in csvd:
        dt = e[1].split(';')
        tail = []
        for e2 in dt:
            dt2 = e2.split(':')
            sec = int(dt2[0])
            pw = float(dt2[1])
            tail.append((sec, pw))

        videvs.append((int(e[0]), tail, int(e[0]) + 5)) 

    cap = cv2.VideoCapture(vidfn)
#    time.sleep(4.5)

    cv2.namedWindow('xx', cv2.WINDOW_NORMAL)
    ctr = 0
    disp_sound = None
    disp_vid = None

    while (cap.isOpened()):
        if ctr % 25 == 0:
            print ctr // 25
        ctr_vid = ctr
#        print ctr, evs[0][0]
        ret, img = cap.read()
        if disp_sound: #long disp_sound
#            print "\t", disp_sound
            if ctr < disp_sound[4]:
#                pass
#                paint_sector(img, sectors, disp_sound[1], disp_sound[3])
#                paint_sector2(img, disp_sound[2], disp_sound[3])
                paint_stripe(img, disp_sound[1], disp_sound[3])
            else:
                disp_sound = None
        else: 
            if ctr >= evs[0][0]:
                xc = min(img.shape[1] - 1, int(img.shape[1] / 2.0 * 
                    (1 - np.cos(evs[0][1] * np.pi/180.0))))
#                print "\t", evs[0][0], len(evs), evs[0], xc
                disp_sound = (evs[0][0], xc, evs[0][1], evs[0][2], evs[0][4])
                a = evs.pop(0)

        
        if disp_vid:
            if ctr_vid < disp_vid[2]:
#                paint_sector(img, sectors, disp_vid[1], disp_vid[2])
#                pass
                for ee in disp_vid[1]:
                    paint_sector(img, sectors, ee[0], ee[1])
            else:
                disp_vid = None
        else:
            if ctr_vid > videvs[0][0]:
                disp_vid = videvs[0]
                videvs.pop(0)
                        

        cv2.imshow('xx', img)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        ctr += 1            


    cap.release()
    cv2.destroyAllWindows()
