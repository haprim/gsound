import sys
import numpy as np

def v_sectorize(bmp, nhsec = 20, nvsec = 20):
    hthr = bmp.shape[1] / nhsec
    vthr = bmp.shape[0] / nvsec

    result = [[] for _ in range(nhsec * nvsec)]
    for i in range(bmp.shape[0]):
        for j in range(bmp.shape[1]):
            sec0 = int(j / hthr)
            sec1 = int(i / vthr)
            sector = sec0 * nhsec + sec1
            result[sector].append(bmp[i,j])
#            print i, j, bmp[i, j], d2 > md, sector
#    quit()            
    return result            


def generate_v_sectors(xres, yres, nhsec = 20, nvsec = 20, leave_edges = False):
    hthr = yres / nhsec
    vthr = xres / nvsec
    result = {i: [] for i in range(nhsec * nvsec)}
    for i in range(xres):        
        if leave_edges:
            if (i < 3) or (i >= xres - 3):
                continue
        for j in range(yres):
#            sector = int(j / thr)
            sec0 = int(j / hthr)
            sec1 = int(i / vthr)
            sector = sec0 * nhsec + sec1
            result[sector].append((i, j))
    return result



def datdiff(d1, d2, thr):
    result = []
    for i in range(len(d1)):
#        print d1
        df = sum(np.abs(np.subtract(d1[i], d2[i])))
        result.append((df, len(d1[i])))
    return result        

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Usage: {0} filename".format(sys.argv[0])
        quit()

    fn = sys.argv[1]

    arr = np.load(fn)
    lastsec = -1

    s0 = v_sectorize(arr[0])

    for i in range(1, len(arr)):
        s1 = v_sectorize(arr[i])
#    print [len(X) for X in sects0]
#    print sects0
        thr = 20
        ddff = datdiff(s1, s0, thr)

        s0 = s1

        active_sectors = []
        for j, e in enumerate(ddff):
            xx = e[0] / e[1]
            if xx > thr:
                active_sectors.append((j, xx))

        if len(active_sectors) > 0:
            mxsloc = np.argmax([x[1] for x in active_sectors])
            asc = active_sectors[mxsloc]
            if asc[0] == lastsec:
                continue
#            print "{0},{1},{2}".format(5 * i, asc[0], asc[1]), active_sectors
            print "{0},{1}".format(5 * i, 
                ';'.join(["{0}:{1}".format(x[0], x[1]) for x in active_sectors]))
#            print "{2}::: SIGNAL IN SECTOR {0}, {1}".format(
#                asc[0], asc[1], 5 * i)
            lastsec = asc[0] 
#            for e in active_sectors:
#                print "TIME={0}, SECTOR={1}".format(5*i, e[0]), e[1]
#            print "\t", e, e[0] / e[1]
