import numpy as np
import sys
import cv2
import traceback

def smoothen(cap, n):
    if n < 1:
        return None

    try:
        imgs = []
        for i in range(n):
            ret, img = cap.read()
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            imgs.append(gray)
        res = np.average(imgs, axis=0)
#        for x in np.nditer(res, op_flags=['readwrite']):
#            x[...] = int(x)

#        print res
#        print res.shape                
        return res

    except:
        traceback.print_exc()
        return None

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Usage: {0} avi_video outfile".format(sys.argv[0])
        quit()

    fn = sys.argv[1]
    cap = cv2.VideoCapture(fn)

    ctr = 0
    dats = []

    while(cap.isOpened()):
        if ctr % 10 == 0:
            print "{0}".format(ctr)
#        print ctr
        bmp = smoothen(cap, 5)
        if bmp == None:
            break
        dats.append(bmp)

#        ret, frame = cap.read()
        """
        print frame.shape
        print frame[20,19]
        print frame[20,39]
        print frame[29,21]
        """
#        quit()

#        gray = cv2.cvtColor(bmp, cv2.COLOR_BGR2GRAY)

        fn = 'out/f{0}.png'.format(ctr)
#        cv2.imwrite(fn, gray)
        ctr += 1
        cv2.imshow('frame',bmp)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    np.save(sys.argv[2], dats)
#    aaa=np.load('xxx.npy')
#    print aaa

    cap.release()
    cv2.destroyAllWindows()
