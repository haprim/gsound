import numpy as np
import sys
import scipy.io.wavfile as wf
import scipy.signal as scs

from indices import loxx

__EPSILON__ = 0.000000000005

def readwave(fn):
    rate, stream = wf.read(fn)
    data0 = stream[:, 0]
    data1 = stream[:, 1]
    assert(len(data0) == len(data1))

    return data0, data1, rate

def normalize(channel, bitrate = 16):
    div = float(2 ** (bitrate - 1))
    channel = channel  / div
    return channel

def get_onset_and_angle(ch1, ch2):
    n = 2 ** 10
    step = n / 2
    nfft = n
    window = 1
    scaleback = 1
    dlimit = 18
    e1rate = 5.0
    e2rate = 20.0

    rectified = np.abs(ch1) + np.abs(ch2)
#    print np.average(rectified)
#    if np.average(rectified) < 0.1:
#        return None, None, None

    b1, a1 = scs.butter(1, e1rate / (SR / 2.0))
    env1 = scs.filtfilt(b1, a1, rectified, padlen = 3)

    b2, a2 = scs.butter(1, e2rate / (SR / 2.0))
    env2 = scs.filtfilt(b2, a2, rectified, padlen = 3)

    envdiff = np.maximum(env2 - 1.1 * env1, 0)

    #find onsets

    larger = False
    pkval = 0
    onsets = []
    peakvalues = []

    delays = []
    first_appear = {}

    for i, e in enumerate(env1[:-1]):
        if (not larger) and (envdiff[i] > 0) and (env1[i] < env1[i + 1]):
            onsets.append(i)
            larger = True
            pkval = 0

        if larger:
            pkval = max(pkval, envdiff[i])

        if (larger) and (envdiff[i] < __EPSILON__): #was ed[i] == 0
            larger = False
            peakvalues.append(pkval)

    if larger: #add last one)
        peakvalues.append(pkval)


    for i, e in enumerate(onsets):
        start = onsets[i] - n / 2
        start = max(start, 0)
        start = min(start, len(ch1) - n - 1)

        left = ch1[start + 1: start + n + 1]
        right = ch2[start + 1: start + n + 1]

        if window:
            wleft = left * scs.hanning(n)
            wright = right * scs.hanning(n)

        lx = np.fft.fft(wleft)
        rx = np.fft.fft(wright)
        corr = np.real(np.fft.ifft(lx * np.conj(rx)))

        corr = np.fft.fftshift(corr)

        if scaleback:
            tr = np.array(range(n / 2)) / float(n / 2)
            itr = np.array(list(reversed(range(n / 2)))) / float(n / 2)

            triangle = np.concatenate((tr, itr))
            invtri = 1.0 / np.maximum(triangle, 0.5)
            corr = corr * invtri

        if dlimit > 0:
            for j in range(n / 2 - dlimit):
                corr[j] = 0.0
            for j in range(n / 2 - dlimit + 1): # FIXME not the same as above?
                corr[n - 1 - j] = 0.0

        maxloc = np.argmax(corr)
        maxval = corr[maxloc]

        ind = maxloc - n / 2
        if abs(ind) > 18:
#            print "STRANGE:", ind, len(corr)
            continue
        delays.append(ind)

        if ind not in first_appear:
            first_appear[ind] = onsets[i]



    hist = np.zeros(2 * dlimit)
    for i, e in enumerate(delays):
        if (e >= -dlimit) and (e < dlimit):
            hist[e + dlimit] += peakvalues[i]

    best = np.argmax(hist)
    abase = best - dlimit + 1
    ab2 = abase - 1
    if len(delays) == 0:
        first_appear[ab2] = 0
        hist[best] = 0

    if abase < -15:
#        print first_appear, delays
        first_appear[-15] = first_appear[ab2]
        abase = -15
    elif abase > 15:
        first_appear[15] = first_appear[ab2]
        abase = 15

    angle = 180.0 * np.arccos(-abase / 15.0) / np.pi

    return first_appear[ab2], angle, hist[best]


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Usage: {0} filename".format(sys.argv[0])
        quit()

    fn = sys.argv[1]

    reso = 25000
    ch1, ch2, SR = readwave(fn)

    fn2 = fn
    ii = fn.rfind('/')
    if ii != -1:
        fn2 = fn[ii + 1:]

    if fn2 not in loxx:
        loclst = [(0, len(ch1))]
    else:
        loclst = loxx[fn2]
        
    ch1 = normalize(ch1)
    ch2 = normalize(ch2)

    loclst = []
    ctr = 0
    while ctr < len(ch1) - reso:
        loclst.append((ctr / float(SR), (ctr + reso)  / float(SR)))
        ctr += reso


    for e in loclst:
        p1 = e[0] * SR
        p2 = e[1] * SR
        ch1a = ch1[p1:p2]
        ch2a = ch2[p1:p2]
        ind, ang, power = get_onset_and_angle(ch1a, ch2a)   
        if ind == None:
            continue
        print "{0},{1},{2},{3},{4}".format((p1 + ind) / SR, ang,power,e[0],e[1])
