% finding distance with cross-correlation
% sliding window  - create a weighted histogram
% more weights are given to data coming from frames
% with stronger cross-correlation
% 
% run in Octave 4.0.0, load signal package beforehand

[data,Fs]=wavread('96khz_onsite1/30f2.wav');
%[data,Fs]=wavread('data/0deg.wav');

%soundsc(data,Fs);


L=length(data);

% preprocessing - usually not needed
%  [B,A]=butter(4,5000/(Fs/2)); % lowpass filtering
%  data(:,1)=filter(B,A,data(:,1));
%  data(:,2)=filter(B,A,data(:,2));
% [B,A]=butter(2,50/(Fs/2),'high'); % highpass filtering
% data(:,1)=filter(B,A,data(:,1));
% data(:,2)=filter(B,A,data(:,2));




% parameters

N=2^12; % size of data window
STEP=N/2; % window stepsize
%NFFT=2*N;
NFFT=N;
WINDOW=1; % we apply a hann window if 1
SCALEBACK=1; % scaling back with the inverse of triangular window
DLIMIT=18; % outside this delay value we don't search for peaks - set ZERO for no limit

K=floor(L/STEP);

clear cmax;
clear delay;
clear corrM;
for k=0:K-4,
    left=data(1+STEP*k:N+STEP*k,1);
    right=data(1+STEP*k:N+STEP*k,2);
    
    if WINDOW,
        left=left.*hann(N);
        right=right.*hann(N);
    end;
    
    LX=fft(left,NFFT);
    RX=fft(right,NFFT);
    corr=real(ifft(LX.*conj(RX)));
    corr=fftshift(corr);
    
    if SCALEBACK, % scale back with the inverse of the triangle window
        triangle=[0:NFFT/2-1]/(NFFT/2);
        triangle(NFFT/2+1:NFFT)=triangle(end:-1:1);
        invtri=1./(max(triangle,0.5)); % maximum scaling is 2
        corr=corr.*invtri.';
    end;
    
    %corr=2*corr./(sum(left.^2)+sum(right.^2)); % scaled correlation - not much better
    
    if DLIMIT>0,
        corr(1:NFFT/2-DLIMIT)=0;
        corr(NFFT/2+DLIMIT:end)=0;
    end;
    
    [val,ind]=max(corr);
    delay(k+1)=ind-NFFT/2;
    cmax(k+1)=val;    
    corrM(:,k+1)=corr;

end;

figure(1);
subplot(3,1,1); % plot data
plot(data,'r');
subplot(3,1,2); % plot estimated delay in the frames
plot(delay);
subplot(3,1,3); % plot peak correlation
plot(cmax);

% create weighted histogram

hist=zeros(1,36);
for k=1:length(delay),
    ind=delay(k)+length(hist)/2;
    if (ind>0) && (ind<length(hist)+1),
        hist(ind)=hist(ind)+cmax(k).^2;
    end;
end;
figure(2);
xscale=[1:length(hist)]-length(hist)/2;
plot(xscale,hist);
grid;



[val,maxind]=max(hist);
maxind=maxind-length(hist)/2;
angle=real(180*acos(-maxind/15)./pi)
