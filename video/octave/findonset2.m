% finding distance with cross-correlation
% at onsets
% 
% run in Octave 4.0.0, load signal package beforehand
pkg load signal


#[data,Fs]=wavread('../../data/s96/0f2.wav');
[data,Fs]=wavread('../s39.wav');
%[data,Fs]=wavread('data/0deg.wav');

% preprocessing - usually not needed
%  [B,A]=butter(4,5000/(Fs/2)); % lowpass filtering
%  data(:,1)=filter(B,A,data(:,1));
%  data(:,2)=filter(B,A,data(:,2));
% [B,A]=butter(2,50/(Fs/2),'high'); % highpass filtering
% data(:,1)=filter(B,A,data(:,1));
% data(:,2)=filter(B,A,data(:,2));

%max(data(:,1))

soundsc(data,Fs); 

L=length(data);

% parameters

N=2^8; % size of data window
STEP=N/2; % window stepsize
%NFFT=2*N;
NFFT=N;
WINDOW=1; % we apply a hann window if 1
SCALEBACK=1; % scaling back with the inverse of triangular window
DLIMIT=18; % outside this delay value we don't search for peaks - set ZERO for no limit



rect=abs(data(:,1))+abs(data(:,2));
[B1,A1]=butter(1,5/(Fs/2)); % slow envelope
env1=filtfilt(B1,A1,rect);

[B2,A2]=butter(1,20/(Fs/2)); % fast envelope
env2=filtfilt(B2,A2,rect);


envdiff=max(env2-1.1*env1,0);

% finding onsets

LARGER=0;
clear onsets;
clear peakvalues;
n=0;
pkval=0;
for k=1:length(env1)-1,
    if ~LARGER && (envdiff(k)>0) && (env1(k)<env1(k+1)),
        n=n+1;
        onsets(n)=k;
        LARGER=1;
        pkval=0;
    end;
    if LARGER==1,
        %pkval=pkval+envdiff(k); % area
        pkval=max(pkval,envdiff(k)); % peak height
    end;
    
    if LARGER && (envdiff(k)==0),
        LARGER=0;
        peakvalues(n)=pkval;
    end;
end;
if LARGER, % if the interval did not end
    peakvalues(n)=pkval;
end;

% plot envelopes

%figure(1);
%subplot(2,1,1);
%plot(rect);
%hold on;
%plot(1.1*env1,'r');
%plot(env2,'k');
%plot(onsets,env2(onsets),'go');
%hold off;

% plot the envelope differences

%subplot(2,1,2);
%plot(envdiff);




clear delay;
clear cmax;
clear corrM;


for n=1:length(onsets);
    
    start=onsets(n)-N/2;
    start=max(start,0);
    start=min(start,length(data)-N-1);
    
    left=data(start+1:start+N,1);
    right=data(start+1:start+N,2);


    if WINDOW,
        wleft=left.*hann(N);
        wright=right.*hann(N);
    end;

    LX=fft(wleft,NFFT);
    RX=fft(wright,NFFT);
    corr=real(ifft(LX.*conj(RX)));
    corr=fftshift(corr);

    if SCALEBACK, % scale back with the inverse of the triangle window
        triangle=[0:NFFT/2-1]/(NFFT/2);
        triangle(NFFT/2+1:NFFT)=triangle(end:-1:1);
        invtri=1./(max(triangle,0.5)); % maximum scaling is 2
        corr=corr.*invtri.';  
    end;


%     subplot(3,1,1);
%     plot(data);
%     hold on;
%     plot(onsets(n),0,'go');
%     hold off;
%     
%     subplot(3,1,2);
%     plot(left);
%     hold on;
%     plot(right,'r');
%     hold off;
%     
%     subplot(3,1,3);
%     plot([-NFFT/2:NFFT/2-1],corr);
%     axis([-30 30 min(corr) max(corr)]);
%     
% 
%     pause(2);

    if DLIMIT>0,
        corr(1:NFFT/2-DLIMIT)=0;
        corr(NFFT/2+DLIMIT:end)=0;
    end;
    
    [val,ind]=max(corr);
    delay(n)=ind-NFFT/2;
    cmax(n)=val;
    

    corrM(:,n)=corr;


end;

% compose histogram    
    
hist=zeros(1,36);
for k=1:length(delay),
    ind=delay(k)+length(hist)/2;
    if (ind>0) && (ind<length(hist)+1),
        %hist(ind)=hist(ind)+cmax(k).^1;
        hist(ind)=hist(ind)+peakvalues(k).^1;
    end;
end;
figure(2);
xscale=[1:length(hist)]-length(hist)/2;
plot(xscale,hist);
grid;

% calculate angle of arrival from the peak

[val,maxind]=max(hist);
maxind
maxind=maxind-length(hist)/2;
angle=real(180*acos(-maxind/15)./pi)

maxind
hist
%aa=input('x')




