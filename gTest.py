import wave
import numpy as np
import sys
import scipy.io.wavfile as wf
from scipy.io.wavfile import write, read
from scipy.signal import get_window, resample, blackmanharris, triang
from scipy.fftpack import fft, ifft, fftshift
from scipy import signal as sgnl
import matplotlib.pyplot as plt
import cmath
import math, copy, sys, os



def stft_stereo(x1, x2, wlen):
   nsampl = x1.size  #sample size

   #Computing STFT coefficients
   #Defining sine window
   win = np.sin(np.pi*(np.arange(.5,wlen-.5+1))/wlen)

   # Zero-padding
   nframe = np.ceil((nsampl/float(wlen))*2)   
   x1 = np.append(x1,np.zeros((nframe*wlen/2)-nsampl))
   x2 = np.append(x2,np.zeros((nframe*wlen/2)-nsampl))

   #Pre-processing for edges   
   x1 = np.append(np.append(np.zeros(wlen/4), x1), np.zeros(wlen/4))
   x2 = np.append(np.append(np.zeros(wlen/4), x2), np.zeros(wlen/4))
   swin=np.zeros((nframe+1)*wlen/2)

   for t in np.arange(0,nframe):
       swin[(t*wlen/2):(t*wlen/2+wlen)] = swin[(t*wlen/2):(t*wlen/2+wlen)]+win**2

   swin=np.sqrt(wlen*swin)
   nbin=wlen/2+1

   X1 = np.zeros([nbin,nframe])
   X2 = np.zeros([nbin,nframe])
   X1 = X1 + 0j
   X2 = X2 + 0j


   for t in np.arange(0,nframe):
           #Framing
           frame1 = (x1[(t*wlen/2):(t*wlen/2+wlen)] *win) /swin[(t*wlen/2):(t*wlen/2+wlen)]
           frame2 = (x2[(t*wlen/2):(t*wlen/2+wlen)] *win) /swin[(t*wlen/2):(t*wlen/2+wlen)]
           #FFT
           fframe1=np.fft.fft(frame1)
           fframe2=np.fft.fft(frame1)
           X1[:,t]=fframe1[0:nbin]
           X2[:,t]=fframe2[0:nbin]      

   return X1 , X2



def readwave(fn):
    rate, stream = wf.read(fn)
    data0 = stream[:, 0]
    data1 = stream[:, 1]
    assert(len(data0) == len(data1))
    return rate, data0, data1

os.chdir("/home/adxpoi/Downloads/001Soundstuff")
inputFile = "m120deg.wav"
(fs, x1, x2) = readwave(inputFile)

d = .05                                            #microphone separation in meters
tau_grid = np.linspace(-d/344.9, d/344.9, 291)     #possible values for time delay of arrival
                                                   #speed of sound at 23 degrees assumed 344.9 m/s


def locate_spect(x1, x2, fs, d, tau_grid):

   #Time-frequency transform 
   wlen = 256

   X1, X2 = stft_stereo(x1, x2, wlen)
   X1= X1[range(1,len(X1[:,0])),:]
   X2= X2[range(1,len(X2[:,0])),:]


   
   f = fs/wlen *np.arange(1, wlen/2 +1)

   #GCC-PHAT Generalized Cross Correlation with phase transform weighting
   spect = PHAT_spect(X1, X2, f, tau_grid)
   spect = np.amax(np.sum(spect,1),axis=1)


   #Finding the peaks (pending)   
   #Computing Tau (pending)  

   return spect




def PHAT_spect(X1, X2, f, tau_grid):
   nbin, nframe  = len(X1[:,0]) , len(X1[0,:])
   ngrid = len(tau_grid)
   
   spect = np.zeros([ngrid, nbin, nframe])

   P = X1 *np.conjugate(X2)
   P = P /abs(P)

   for ind in np.arange(0,ngrid):      
      EXP =  (np.tile((np.exp(-2*1j*np.pi*tau_grid[ind]*f)), (nframe,1))).T
      spect[ind] = np.real(P *EXP)

   return spect


